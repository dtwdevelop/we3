<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    protected $fillable = ['company', 'name', 'email', 'phone', 'message', 'hash', 'status'];

    public function pdf()
    {
        return $this->hasOne("App\Pfd", 'contact_id');
    }

    public function orderStatus()
    {
        return $this->hasOne("App\OrderStatus", 'id', 'status');
    }

    public function quoteForm()
    {
        return $this->hasOne("App\QuoteForm", 'id', 'quote_form_id');
    }

    public function priceOffer()
    {
        return $this->hasOne("App\PriceOffer", 'id', 'price_offer_id');
    }

}
