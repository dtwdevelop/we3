<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Contact;
use App\FormConfig;
use Intervention\Image\Facades\Image;
use File;
use Mail;
use Zofe\Rapyd\Facades\DataSet;
use App\QuoteForm;
use DB;
use Exception;

//use Zofe\Rapyd\Facades\DataForm;
//use Zofe\Rapyd\Facades\DataEdit;
use Zofe\Rapyd\Facades\DataGrid;
use Zofe\Rapyd\Facades\DataFilter;

class ContactController extends AdminController
{
    /**
     * REQUESTS
     */
    public function index()
    {

        $filter = DataFilter::source(Contact::where('is_deleted', 0)
            ->with("pdf")->with('quoteForm')->with('priceOffer'));

        /*$filter = DataFilter::source(
            DB::table('contacts')
            ->leftJoin('order_statuses', 'contacts.status', '=', 'order_statuses.id')
            ->leftJoin('quote_forms', 'contacts.quote_form_id', '=', 'quote_forms.id')
            ->select(
                'contacts.id AS id',
                'contacts.company AS company',
                'contacts.name AS name',
                'contacts.email AS email',
                'contacts.phone AS phone',
                'order_statuses.name_en AS status',
                'quote_forms.hash AS quote_form_hash'
            )
            ->get()
        );*/

        $filter->add('id', 'ID', 'text');
        $filter->add('company', 'Company', 'text');
        $filter->add('name', 'Name', 'text');
        $filter->add('email', 'Email', 'text');
        $filter->submit('search');
        $filter->build();
        $grid = DataGrid::source($filter);

        $grid->add('id', 'ID', true);
        $grid->add('company', 'Company', true);
        /*$grid->add('message', 'Message', true)->cell(function ($value) {
            return nl2br(str_limit($value, 150));
        });*/
        $grid->add('name', 'Name', true);
        $grid->add('email', 'Email', true);
        $grid->add('phone', 'Phone', true);
        $grid->add('date', 'Date', true);
        $grid->add('quote_form', 'Quote Form');
        $grid->add('price_offer', 'Price Offer');
        $grid->add('price_offer_status', 'Status', true);
        $grid->add('actions', 'Actions');

        $grid->orderBy('id', 'desc'); //default orderby
        $grid->paginate(10); //pagination

        $grid->row(function ($row) {

            if ($row->data->price_offer_status == 0) {
                $row->cell('price_offer_status')->value = '';
            } elseif ($row->data->price_offer_status == 1) {
                $row->cell('price_offer_status')->value = 'Sent';
            } elseif ($row->data->price_offer_status == 2) {
                $row->cell('price_offer_status')->value = '<span style="color: green">Accepted</span>';
            } elseif ($row->data->price_offer_status == 3) {
                $row->cell('price_offer_status')->value = '<span style="color: red">Rejected</span>' .
                    ' <a class="btn btn-xs btn-danger"' .
                    'href="/admin/contact/view-complaint/' . $row->data->id . '">' .
                    'Read</a>';
            }

            $row->cell('date')->value = $row->data->created_at->format(config('app.date_format'));

            if ($row->data->quote_form_id === null) {
                $row->cell('quote_form')->value =
                    '<a class="btn btn-xs btn-success"' .
                    "onclick=\"return confirm('Create and send new Quote form to " .
                    $row->data->email . " (" . $row->data->company . ")?')\"" .
                    'href="/admin/contact/send-quote-form/' . $row->data->id . '">' .
                    'Send Form</a>';
            } else {
                if ($row->data->quoteForm->isFilled()) {
                    $row->cell('quote_form')->value =
                        '<a class="btn btn-xs btn-primary"' .
                        'href="' . $row->data->quoteForm->getPdfUrl() . '" target="_blank">' .
                        'View PDF</a>';
                } else {
                    $row->cell('quote_form')->value =
                        '<a class="btn btn-xs btn-warning"' .
                        "onclick=\"return confirm('Resend quote form to " .
                        $row->data->email . " (" . $row->data->company . ") again?')\"" .
                        'href="/admin/contact/resend-quote-form/' . $row->data->id . '">' .
                        'Resend</a>';
                }

            }

            if ($row->data->price_offer_id === null) {
                $row->cell('price_offer')->value =
                    "<a class='btn btn-xs btn-success' " .
                    "href='/admin/price_offers/create/{$row->data->id}' >" .
                    "Create Offer</a>";
            } else {
                if ($row->data->price_offer_status != 2) {
                    $row->cell('price_offer')->value =
                        "<a class='btn btn-xs btn-info' " .
                        "href='/admin/price_offers/{$row->data->price_offer_id}/edit'>" .
                        "Edit</a>";
                }

                $row->cell('price_offer')->value .=
                    " <a class='btn btn-xs btn-primary' " .
                    "href='/price_offer/{$row->data->priceOffer->hash}' target='_blank'>" .
                    "View</a> ";

                if ($row->data->price_offer_status != 2) {
                    if ($row->data->priceOffer->is_sent === null || $row->data->priceOffer->is_sent == 0) {
                        $row->cell('price_offer')->value .=
                            " <a class='btn btn-xs btn-primary' " .
                            "onclick=\"return confirm('Send Price Offer to " .
                            $row->data->email . " (" . $row->data->company . ")?')\"" .
                            "href='/admin/price_offers/{$row->data->price_offer_id}/send'>" .
                            "Send</a>";
                    } else {
                        $row->cell('price_offer')->value .=
                            " <a class='btn btn-xs btn-warning' " .
                            "onclick=\"return confirm('Resend Price Offer to " .
                            $row->data->email . " (" . $row->data->company . ") again?')\"" .
                            "href='/admin/price_offers/{$row->data->price_offer_id}/resend'>" .
                            "Resend</a>";
                    }
                }
            }

            $row->cell('actions')->value =
                "<a class='btn btn-xs btn-success'" .
                " href='/admin/contact/{$row->data->id}' >" .
                "Read</a>";

            if ($row->data->price_offer_status != 2) {
                $row->cell('actions')->value .= ' <a class="btn btn-xs btn-danger"' .
                    "onclick=\"return confirm('Are you sure you want to delete message from " .
                    $row->data->email . " (" . $row->data->company . ")?')\"" .
                    'href="/admin/contact/' . $row->data->id . '/delete">' .
                    'Delete</a>';
            }


            /**
             * check if fill pdf form
             */
            /*if ($row->data->pdf != null) {
                if ($row->data->pdf->fill != null) {
                    //'<a class="btn btn-xs btn-primary" href="/pdf/user/$row->data->pdf->contact_id.pdf">Pdf show</a>';
                    $row->cell('pdf.fill')->value =
                        "<a class='btn btn-xs btn-primary' href='/pdf/user{$row->data->id}.pdf'>Show PDF</a>";
                }
            } else {
                $row->cell('pdf.fill')->value = 'Waiting';
            }*/

        });

        //$grid->add('pdf.fill', 'Pdf file');

        //$grid->edit('/admin/contact/destroy/', 'Delete', 'delete'); //shortcut to link DataEdit actions
        /*$grid->add(
            '<a class="btn btn-xs btn-danger" href="/admin/contact/sendform/{{ $id }}">Send Pdf</a>',
            'Pdf file'
        );
        $grid->add(
            '<a class="btn btn-xs btn-warning" href="/admin/contact/sendconfirm/{{$id}}">Send Confirm</a>',
            'Confirm'
        );*/


        return view('admin.contact.list', ['title' => 'Contact', "grid" => $grid, 'filter' => $filter]);
    }

    public function getDelete($id)
    {
        $contact = Contact::find($id);
        if ($contact !== null) {
            $contact->is_deleted = 1;
            $contact->save();
        }

        return redirect('/admin/contact');
    }

    /**
     * /END REQUESTS
     * QUOTE FORM API
     */
    protected function sendQuoteFormEmail($contact, $hash)
    {
        $mail_sent = Mail::send(
            'emails.quote_form',
            array('name' => $contact->name, 'hash' => $hash),
            function ($message) use ($contact) {
                $message->from(config('mail.username'), config('mail.label'));
                $message->to($contact->email, $contact->name);
                $message->subject(sprintf(config('mail.subject_quote_form'), $contact->company));
            }
        );
        if (!$mail_sent) {
            return false;
        }

        return true;
    }

    /**
     * /END QUOTE FORM API
     * QUOTE FORM REQUESTS
     */
    public function getSendQuoteForm($id)
    {
        $contact = Contact::find($id);
        if ($contact === null) {
            return redirect('/admin/contact')->with(
                [
                    'message' => 'Message with ID ' . $id . ' was not found!',
                    'message-type' => 'danger'
                ]
            );
        } else {
            try {
                DB::transaction(function () use ($contact) {

                    // 1. generate new quote form in DB
                    $quote_form = new QuoteForm();
                    $quote_form->hash = sha1('quote_form' . $contact->id . time());
                    $quote_form->contact_id = $contact->id;
                    $quote_form->is_filled = 0;
                    $saved = $quote_form->save();
                    if (!$saved) {
                        throw new Exception('Error creating DB record for model QuoteForm');
                    }

                    // 2. send email to client
                    $mail_sent = $this->sendQuoteFormEmail($contact, $quote_form->hash);
                    if (!$mail_sent) {
                        throw new Exception('Error sending mail to client');
                    }

                    // 3. set status to 1 (quote form sent)
                    $contact->status = 1;
                    $contact->quote_form_id = $quote_form->id;
                    $contact_saved = $contact->save();
                    if (!$contact_saved) {
                        throw new Exception('Error updating DB record for model Contact');
                    }

                });

                return redirect('/admin/contact')->with(
                    [
                        'message' => 'Quote Form have been sent to client "' . $contact->company . '" email "' .
                            $contact->email . '"'
                    ]
                );
            } catch (Exception $e) {
                return redirect('/admin/contact')->with(
                    [
                        'message' => $e->getMessage(),
                        'message-type' => 'danger'
                    ]
                );
            }

        }
    }

    public function getResendQuoteForm($id)
    {
        $contact = Contact::find($id);

        if ($contact === null) {
            return redirect('/admin/contact')->with(
                [
                    'message' => 'Message with ID ' . $id . ' was not found!',
                    'message-type' => 'danger'
                ]
            );
        } else {
            try {
                // send email to client
                $mail_sent = $this->sendQuoteFormEmail($contact, $contact->quoteForm->hash);
                if (!$mail_sent) {
                    throw new Exception('Error sending mail to client');
                }
            } catch (Exception $e) {
                return redirect('/admin/contact')->with(
                    [
                        'message' => $e->getMessage(),
                        'message-type' => 'danger'
                    ]
                );
            }

            return redirect('/admin/contact')->with(
                [
                    'message' => 'Quote Form have been sent again to client "' . $contact->company . '" email "' .
                        $contact->email . '"'
                ]
            );
        }
    }
    /**
     * /END QUOTE FORM REQUESTS
     */

    public function get($id)
    {
        $contact = Contact::find($id);

        if ($contact === null) {
            return redirect('/admin/contact')->with(
                [
                    'message' => 'Message with ID ' . $id . ' was not found!',
                    'message-type' => 'danger'
                ]
            );
        }

        return view('admin/contact/view', compact('contact'));
    }

    public function getViewComplaint($id)
    {
        $contact = Contact::find($id);

        if ($contact === null) {
            return redirect('/admin/contact')->with(
                [
                    'message' => 'Message with ID ' . $id . ' was not found!',
                    'message-type' => 'danger'
                ]
            );
        }

        return view('admin/contact/view_complaint', compact('contact'));
    }
}
