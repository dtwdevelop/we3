<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Service;

class DevNullController extends AdminController {

    public function __construct()
    {
        parent::__construct();
    }

    public function getServices(){
        $services = Service::all();
        return view('admin.dev.null.services', compact('services'));
    }

}
