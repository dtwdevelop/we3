<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\ClientMessage;
use Intervention\Image\Facades\Image;
use File;
use App\Contact;
use App\PriceOffer;

class MessagesController extends AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
//    public function __construct() {
//        $this->middleware('auth', [ 'except' => [ 'index', 'show' ] ]);
//    }




    public function index() {


    }
    
    


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
 
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $message = Contact::find($id);
         $client = ClientMessage::where('contact_id',$id)->get();
         //dd($client);
          return view('admin.client.message',['messages'=>$client,'contact'=>$message]);
    }

    public function show2($id) {

        $messages = ClientMessage::where('price_id',$id)->orderBy('created_at','desc')->get();
        $conatct= PriceOffer::find($id);
        return view('admin.client.message2',['messages'=>$messages,'contact'=>$conatct]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
