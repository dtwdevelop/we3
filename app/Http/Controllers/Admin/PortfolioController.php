<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Portfolio;
use Intervention\Image\Facades\Image;
use File;
use App\Portfolio_Upload;

class PortfolioController extends AdminController {

    const UPLOAD_URL='appfiles/portfolio';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
//    public function __construct() {
//        $this->middleware('auth', [ 'except' => [ 'index', 'show' ] ]);
//    }




    public function index() {


        $logos = Portfolio::orderBy('title', 'asc')->paginate(10);
        if(File::exists('portfolio/logo/logo2')) {
         $files = File::files('portfolio/logo/logo1');
        // dd($files);
}
      

        return view('admin.portfolio.index', ['title' => 'Porfolio', "logos" => $logos]);
    }
    
     public function showfoto($id) {
       $logo =  Portfolio::find($id);
       if($logo != null){
            if(File::exists(self::UPLOAD_URL.'/'.$logo->map)) {
        $files = File::files(self::UPLOAD_URL.'/'.$logo->map);
         //dd($files);
          return view('admin.portfolio.gallery', ['files'=>$files]);
       }
       else{
           abourt(404,"No photos here");
       }
        
       }

      

      
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('admin.portfolio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
      // dd($request->all());
//       
//       $attrs =   ["title"=>"Sample",
//          'description' => 'First',
//           'mapname' => 'Logo 1',
//           'hide' => 0,
//          ];
//        Portfolio::create($attrs);
        
//        $img = Image::make('public/car.jpg');
//        $img->resize(200, 200);
//        $img->save('public/logo/logo.jpg');
       // $file = $request->file('logo')->move('public/logo');
         
        $this->validate($request, [
            'title' => 'required|max:200',
            'description' => 'required',
           
            'map' => 'required|max:10',
            'logo' => 'required|image',
            'url'=>'url',
        ]);
        
        if($request->hasFile('logo')){
            
           
            //UPLOAD_URL
            $filename = $request->file('logo')->getClientOriginalName();
            $logoname = $filename.str_random(10).".jpg";
            $file = $request->file('logo')->move(self::UPLOAD_URL.'/',$logoname);
            $folder = $request->input('map');
            $attr = ['title'=>$request->input('title'),
                 'description' => $request->input('description'),
                 'map' => $folder,
                 'logo'=>$logoname,
                 'url'=>$request->get("url"),
                 'hide'=>0
                
                ];
             Portfolio::create($attr);
            File::makeDirectory(self::UPLOAD_URL.'/'.$folder, 0775, true, true);
//         $img = Image::make('public/car.jpg');
//        $img->resize(200, 200);
//        $img->save('public/logo/logo.jpg');
             
           return  redirect('admin/portfolio');
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
       
    }
    
    public function fotolist($id){
        $fotos = Portfolio_Upload::where(['portfolios_id'=>$id])->get();
        $folder= Portfolio::find($id);
        
       
        return view('admin.portfolio.fotos',['fotos'=>$fotos,"folder"=>$folder]);
    }
    
    public function uploadfoto(Request $request){
        
        // dd($request);
            $id = $request->input('id');
            $logo =  Portfolio::find($id);
            $filename = $request->file('foto')->getClientOriginalName();
            $logoname = $filename.str_random(10).".jpg";
            $file = $request->file('foto')->move(self::UPLOAD_URL.'/'. $logo->map.'/',$logoname);
           
            
            $foto = new Portfolio_Upload();
            $foto->portfolios_id =$id;
            $foto->title =$logoname;
            $foto->save();
           return redirect('admin/portfolio')->with('photo','Created new photo');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
         $logo =  Portfolio::find($id);
         return view("admin.portfolio.update",['model'=>$logo]);
    }
    public function fotodelete($id){
         $logo =  Portfolio_Upload::find($id);
          $map =  Portfolio::find($logo->portfolios_id);
       if(File::exists(self::UPLOAD_URL."/$map->map/$logo->title")) {
          File::delete(self::UPLOAD_URL."/$map->map/$logo->title");
         
          $logo->delete();
       
       }
        return  redirect('admin/portfolio')->with('photo','Photo Edit');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request) {
        $this->validate($request, [
            'title' => 'required|max:200',
            'description' => 'required',
           
            'map' => 'required|max:10',
            'logo' => 'required|image',
            'url'=>'url',
            
        ]);
        
        if($request->hasFile('logo')){
            
           
             $logo =  Portfolio::find($id);
       if(File::exists(self::UPLOAD_URL."/$logo->logo")) {
          File::delete(self::UPLOAD_URL."/$logo->logo");
         
          $logo->delete();
       
       }
            $filename = $request->file('logo')->getClientOriginalName();
            $logoname = $filename.str_random(10).".jpg";
            $file = $request->file('logo')->move(self::UPLOAD_URL.'/',$logoname);
            $folder = $request->input('map');
            $attr = ['title'=>$request->input('title'),
                 'description' => $request->input('description'),
                 'map' => $folder,
                 'logo'=>$logoname,
                 'url'=>$request->get("url"),
                 'hide'=>0
                
                ];
             Portfolio::create($attr);
          //  File::makeDirectory(self::UPLOAD_URL.'/'.$folder, 0775, true, true);
//         $img = Image::make('public/car.jpg');
//        $img->resize(200, 200);
//        $img->save('public/logo/logo.jpg');
             
           return  redirect('admin/portfolio')->with('photo','Logo updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
       $logo =  Portfolio::find($id);
       if(File::exists(self::UPLOAD_URL."/$logo->logo")) {
          File::delete(self::UPLOAD_URL."/$logo->logo");
          File::deleteDirectory(self::UPLOAD_URL."/$logo->map");
          $logo->delete();
       
       }
        return  redirect('admin/portfolio')->with('photo','Logo deteleted');
    }

}
