<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\PriceOfferRequest;
use App\Http\Requests\Admin\PriceOfferEditRequest;
use App\PriceOffer;
use App\Service;
use DB;
use App;
use App\Contact;
use Exception;
use Redirect;
use App\OrderStatus;
use App\Portfolio;
use Mail;

class PriceOffersController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $prices = PriceOffer::where('contact_id',0)->paginate(10);

        return view('admin.price_offers.index', compact('prices'));
    }

    public function getCreate($contact_id)
    {

        $contact = Contact::find($contact_id);
        if ($contact === null || $contact->price_offer_id !== null) {
            App::abort(404);
        }

        $price_offer = new PriceOffer();
        $price_offer->services = array(new Service);
        $price_offer->portfolios = array(new Portfolio);
        $price_offer->fillWithContactFields($contact);

       // $statuses = OrderStatus::all();
        $services = Service::all();
        $portfolios = Portfolio::all();

        return view(
            'admin.price_offers.create_edit',
            compact('services', 'price_offer', 'statuses', 'contact', 'portfolios')
        );
    }

    public function getCreatePrice()
    {
        $price_offer = new PriceOffer();
        $price_offer->services = array(new Service);
        $price_offer->portfolios = array(new Portfolio);
        $services = Service::all();
        $portfolios = Portfolio::all();
        return view(
            'admin.price_offers.create_edit2',
            compact('services', 'price_offer', 'statuses', 'contact', 'portfolios')
        );
    }

    public function postCreate2(PriceOfferRequest $request, $contact_id)
    {
        if ($request->is_viewable === null) {
            $request->is_viewable = 0;
        }

        $price_offer = new PriceOffer();
        $price_offer->fill($request->all());
        $price_offer->contact_id = 0;
        $price_offer->doc_nr = 'WE3-15/' . $request->input('doc_nr');
        $price_offer->price_offer_status=0;
        try {
            $res = DB::transaction(function () use ($price_offer, $request) {

                $price_offer->hash = sha1($price_offer->doc_nr . '/' . time());
                $price_offer->save();
                $price_offer->insertOrUpdateServices($request);
                $price_offer->insertOrUpdatePortfolios($request);
            });

            if ($res === null) {
                return redirect()->intended('admin/price_offers')
                    ->with('message', sprintf(trans('admin/price_offers.message_created_ok'), $request->doc_nr));
            } else {
                return redirect()
                    ->with('message', trans('admin/price_offers.message_created_fail'))
                    ->withInput();
            }
        } catch (Exception $e) {
            //dd($request->all());
            return Redirect::back()
                ->withErrors(['message' => $e->getMessage()])
                ->withInput();
        }
    }

    public function getEdit2($id)
    {
        $price_offer = PriceOffer::find($id);

        if ($price_offer === null) {
            App::abort(404);
        }

        $contact = Contact::find($price_offer->contact_id);
        //$price_offer->fillWithContactFields($contact);
        $price_offer->loadServices();
        $price_offer->loadPortfolios();

        // $statuses = OrderStatus::all();
        $services = Service::all();
        $portfolios = Portfolio::all();

        return view(
            'admin.price_offers.create_edit2',
            compact('services', 'price_offer', 'statuses', 'contact', 'portfolios')
        );
    }

    public function postCreate(PriceOfferRequest $request, $contact_id)
    {

        $contact = Contact::find($contact_id);
        if ($contact === null || $contact->price_offer_id !== null) {
            App::abort(404);
        }

        if ($request->is_viewable === null) {
            $request->is_viewable = 0;
        }

        $price_offer = new PriceOffer();

        $price_offer->fill($request->all());
        $price_offer->contact_id = $contact->id;
        try {
            $res = DB::transaction(function () use ($price_offer, $request, $contact) {
                $price_offer->hash = sha1($price_offer->doc_nr . '/' . time());
                $price_offer->save();
                $price_offer->insertOrUpdateServices($request);
                $price_offer->insertOrUpdatePortfolios($request);
                $contact->status = 2;
                $contact->price_offer_id = $price_offer->id;

                $contact->save();
            });

            if ($res === null) {
                return redirect()->intended('admin/contact')
                    ->with('message', sprintf(trans('admin/price_offers.message_created_ok'), $request->doc_nr));
            } else {
                return redirect()
                    ->with('message', trans('admin/price_offers.message_created_fail'))
                    ->withInput();
            }
        } catch (Exception $e) {
            //dd($request->all());
            return Redirect::back()
                ->withErrors(['message' => $e->getMessage()])
                ->withInput();
        }
    }

    public function getEdit($id)
    {
        $price_offer = PriceOffer::find($id);

        if ($price_offer === null && $price_offer->contact->status !== 4) {
            App::abort(404);
        }
        $contact = Contact::find($price_offer->contact_id);
        //$price_offer->fillWithContactFields($contact);
        $price_offer->loadServices();
        $price_offer->loadPortfolios();

       // $statuses = OrderStatus::all();
        $services = Service::all();
        $portfolios = Portfolio::all();

        return view(
            'admin.price_offers.create_edit',
            compact('services', 'price_offer', 'statuses', 'contact', 'portfolios')
        );
    }

    public function postEdit2(PriceOfferEditRequest $request, $id)
    {

        $price_offer = PriceOffer::find($id);

        if ($price_offer === null) {
            App::abort(404);
        }

        $price_offer->fill($request->all());
        if ($request->is_viewable === null) {
            $price_offer->is_viewable = 0;

        }

        try {
            $res = DB::transaction(function () use ($price_offer, $request) {
                $price_offer->doc_nr = $request->input('doc_nr');
                $price_offer->save();
                $price_offer->insertOrUpdateServices($request);
                $price_offer->insertOrUpdatePortfolios($request);
            });

            if ($res === null) {
                return redirect()->intended('admin/price_offers')
                    ->with('message', sprintf(trans('admin/price_offers.message_edited_ok'), $request->doc_nr));
            } else {
                return redirect()
                    ->with('message', trans('admin/price_offers.message_created_fail'))
                    ->withInput();
            }
        } catch (Exception $e) {
            return Redirect::back()
                ->withErrors(['message' => $e->getMessage()])
                ->withInput();
        }
    }


    public function postEdit(PriceOfferEditRequest $request, $id)
    {

        $price_offer = PriceOffer::find($id);

        if ($price_offer === null && $price_offer->contact->status !== 4) {
            App::abort(404);
        }

        $price_offer->fill($request->all());
        if ($request->is_viewable === null) {
            $price_offer->is_viewable = 0;
        }

        try {
            $res = DB::transaction(function () use ($price_offer, $request) {
                $price_offer->save();
                $price_offer->insertOrUpdateServices($request);
                $price_offer->insertOrUpdatePortfolios($request);
            });

            if ($res === null) {
                return redirect()->intended('admin/contact')
                    ->with('message', sprintf(trans('admin/price_offers.message_edited_ok'), $request->doc_nr));
            } else {
                return redirect()
                    ->with('message', trans('admin/price_offers.message_created_fail'))
                    ->withInput();
            }
        } catch (Exception $e) {
            return Redirect::back()
                ->withErrors(['message' => $e->getMessage()])
                ->withInput();
        }
    }

    public function getSend($id)
    {
        $price_offer = PriceOffer::find($id);

        if ($price_offer === null) {
            return redirect('/admin/contact')->with(
                [
                    'message' => 'Price Offer with ID '.$id.' was not found!',
                    'message-type' => 'danger'
                ]
            );
        }

        if ($price_offer->contact->price_offer_status === 2) {
            return redirect('/admin/contact')->with(
                [
                    'message' => 'Price Offer with ID '.$id.' was accepted!',
                    'message-type' => 'danger'
                ]
            );
        }

        // Send mail to client
        try {
            DB::transaction(function () use ($price_offer) {

               // $price_offer->status_id = 3;
                $price_offer->is_sent = 1;
                $price_offer->save();
                $price_offer->contact->status = 3;
                $price_offer->contact->price_offer_status = 1;
                $price_offer->contact->save();

                Mail::send(
                    'emails.price_offer',
                    array('name' => $price_offer->contact_person_name, 'hash' => $price_offer->hash),
                    function ($message) use ($price_offer) {
                        $message->from(config('mail.username'), config('mail.label'));
                        $message->to(
                            $price_offer->contact_email,
                            $price_offer->contact_person_name
                        )->subject(sprintf(config('mail.subject_price_offer'), $price_offer->company_name));
                    }
                );
            });
        } catch (Exception $e) {
            return redirect()->intended('admin/contact')
                ->with([
                    'message' => $e->getMessage(),
                    'message-type' => 'danger'
                ]);
        }

        return redirect()->intended('admin/contact')
            ->with([
                'message' => 'Price Offer have been sent to client "'.$price_offer->company_name
                    .'" ('.$price_offer->contact_email.')'
            ]);
    }


    public function getSend2($id)
    {
        $price_offer = PriceOffer::find($id);

        if ($price_offer === null) {
            return redirect('/admin/contact')->with(
                [
                    'message' => 'Price Offer with ID '.$id.' was not found!',
                    'message-type' => 'danger'
                ]
            );
        }

        if ($price_offer->price_offer_status === 2) {
            return redirect('/admin/price_offers')->with(
                [
                    'message' => 'Price Offer with ID '.$id.' was accepted!',
                    'message-type' => 'danger'
                ]
            );
        }

        // Send mail to client
        try {
            DB::transaction(function () use ($price_offer) {
                $price_offer->is_sent = 1;
                $price_offer->save();
                Mail::send(
                    'emails.price_offer',
                    array('name' => $price_offer->contact_person_name, 'hash' => $price_offer->hash),
                    function ($message) use ($price_offer) {
                        $message->from(config('mail.username'), config('mail.label'));
                        $message->to(
                            $price_offer->contact_email,
                            $price_offer->contact_person_name
                        )->subject(sprintf(config('mail.subject_price_offer'), $price_offer->company_name));
                    }
                );
            });
        } catch (Exception $e) {
            return redirect()->intended('admin/contact')
                ->with([
                    'message' => $e->getMessage(),
                    'message-type' => 'danger'
                ]);
        }

        return redirect()->intended('admin/price_offers')
            ->with([
                'message' => 'Price Offer have been sent to client "'.$price_offer->company_name
                    .'" ('.$price_offer->contact_email.')'
            ]);
    }


    public function getResend($id)
    {
        $price_offer = PriceOffer::find($id);

        if ($price_offer === null) {
            return redirect('/admin/contact')->with(
                [
                    'message' => 'Price Offer with ID '.$id.' was not found!',
                    'message-type' => 'danger'
                ]
            );
        }

        if ($price_offer->contact->price_offer_status === 2) {
            return redirect('/admin/contact')->with(
                [
                    'message' => 'Price Offer with ID '.$id.' was accepted!',
                    'message-type' => 'danger'
                ]
            );
        }

        // Send mail to client
        try {
            Mail::send(
                'emails.price_offer',
                array('name' => $price_offer->contact_person_name, 'hash' => $price_offer->hash),
                function ($message) use ($price_offer) {
                    $message->from('info@we3uk.com');

                    $message->to(
                        $price_offer->contact_email,
                        $price_offer->contact_person_email
                    )->subject($price_offer->company_name . " - Price Offer - WE 3 Team");
                }
            );
        } catch (Exception $e) {
            return redirect()->intended('admin/contact')
                ->with([
                    'message' => $e->getMessage(),
                    'message-type' => 'danger'
                ]);
        }

        return redirect()->intended('admin/contact')
            ->with([
                'message' => 'Price Offer have been sent again to client "'.$price_offer->company_name
                    .'" ('.$price_offer->contact_email.')'
            ]);
    }
}
