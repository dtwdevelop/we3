<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\AdminController;

use App\Http\Requests;
use App\Contact;



use App\Project;
use Illuminate\Http\Request;

class ProjectController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = Project::all();

		return view('projects.index', compact('projects'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
                $project = Contact::lists('name','id');
		
		return view('projects.create',['projects'=>$project]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$project = new Project();

		$project->title = $request->input("title");
                $project->contact_id = $request->input("project_id");
		$project->save();

		return redirect()->route('admin.projects.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$project = Project::findOrFail($id);

		return view('projects.show', compact('project'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$project = Project::findOrFail($id);
                 $projects = Contact::lists('name','id');
		return view('projects.edit', compact('project','projects'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$project = Project::findOrFail($id);

		$project->title = $request->input("title");

		$project->save();

		return redirect()->route('admin.projects.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$project = Project::findOrFail($id);
		$project->delete();

		return redirect()->route('admin.projects.index')->with('message', 'Item deleted successfully.');
	}

}
