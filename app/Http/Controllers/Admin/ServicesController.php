<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Service;
use App\Http\Requests\Admin\ServiceRequest;
use App\Http\Requests\Admin\ServiceEditRequest;
use App\ServiceDescription;
use Datatables;

class ServicesController extends AdminController {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $services = Service::all();
        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        $title = trans('admin/services.add_service');
        return view('admin.services.create_edit', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(ServiceRequest $request) {
        $service = new Service();
        $this->createOrEdit($request, $service);
        return redirect()->intended('admin/services')
            ->with('message', sprintf(trans('admin/services.message_created_ok'), $request->title_en));
    }

    protected function createOrEdit($request, Service $service){

        $service -> title_en = $request->title_en;
        $service -> title_ru = $request->title_ru;
        $service -> title_lv = $request->title_lv;

        $icon_file = "";
        if($request->hasFile('icon'))
        {
            $file = $request->file('icon');
            $filename = $file->getClientOriginalName();
            $extension = $file -> getClientOriginalExtension();
            $icon_file = sha1($filename . time()) . '.' . $extension;
            $service -> icon = $icon_file;
        }

        $service -> save();

        if($request->hasFile('icon'))
        {
            $destinationPath = public_path() . '/appfiles/services/'.$service->id.'/';
            $request->file('icon')->move($destinationPath, $icon_file);
        }

        $service->insertOrUpdateDescriptions($request);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $service
     * @return Response
     */
    public function getEdit($id) {

        $service = Service::find($id);
        $title = trans('admin/services.edit_service') . ' "' . $service->title_en . '"';
        return view('admin.services.create_edit', compact('service', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $service
     * @return Response
     */
    public function postEdit(ServiceEditRequest $request, $id) {
        $service = Service::find($id);
        $this->createOrEdit($request, $service);
        return redirect()->intended('admin/services')
            ->with('message', sprintf(trans('admin/services.message_edited_ok'), $service->title_en));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param $service
     * @return Response
     */

    public function getDelete($id)
    {
        $service = Service::find($id);
        $service->delete();
        // Show the page
        return redirect()->intended('admin/services')
            ->with('message', sprintf(trans('admin/services.message_deleted_ok'), $service->title_en));
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
        $services = Service::select(array('services.id','services.title_en','services.title_ru','services.title_lv', 'services.created_at'));

        return Datatables::of($services)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/services/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ trans("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'admin/services/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>
                ')
            ->remove_column('id')

            ->make();
    }
}
