<?php 
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Project;

use App\Timeline;
use Illuminate\Http\Request;

class TimelineController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
               $timelines = Timeline::all();
               
              
		return view('timelines.index', compact('timelines','datatime'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
             $project = Project::lists('title','id');
		return view('timelines.create',['projects'=>$project]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
               $this->validate($request, [
            'title' => 'required|max:200',
            'project_id' => 'required',
           
            'start' => 'required',
            'end' => 'required',
            
        ]);
		$timeline = new Timeline();

		$timeline->title = $request->input("title");
                $timeline->project_id = $request->input("project_id");
                $timeline->start= $request->input("start");
                $timeline->end= $request->input("end");
		$timeline->save();

		return redirect()->route('admin.timelines.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$timeline = Timeline::findOrFail($id);

		return view('timelines.show', compact('timeline'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
             $projects = Project::lists('title','id');
		$timeline = Timeline::findOrFail($id);

		return view('timelines.edit', compact('timeline','projects'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
            
                 $this->validate($request, [
            'title' => 'required|max:200',
            'project_id' => 'required',
           
            'start' => 'required',
            'end' => 'required',
            
        ]);
		$timeline = Timeline::findOrFail($id);

		$timeline->title = $request->input("title");
                $timeline->project_id = $request->input("project_id");
                $timeline->start= $request->input("start");
                $timeline->end= $request->input("end");
		$timeline->save();

		return redirect()->route('admin.timelines.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$timeline = Timeline::findOrFail($id);
		$timeline->delete();

		return redirect()->route('admin.timelines.index')->with('message', 'Item deleted successfully.');
	}

}
