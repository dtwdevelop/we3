<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Contact;
use App\ClientMessage;
use App\Portfolio;
use Mail;
use PDF;
use App;
use File;
use App\Project;
use App\FormConfig;
use App\Pfd as Pdfstatus;

class ContactController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $data = FormConfig::all()->first();

        // dd($data->phone);
        return view('contact.contact', ['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $this->validate($request, [
                'subject' => 'required|max:200',
                'name' => 'required|max:150',
                'email' => 'required|email',
                'phone' => 'required',
                'message' => 'required',
            ]);

            //save
            //send mail
            $hash = str_random(10);
            $attr = [
                'company' => $request->input('subject'),
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'hash' => $hash,
                'message' => $request->get("message"),
                'status' => 0,

            ];

            try {
                DB::transaction(function () use ($attr) {
                    $contact = Contact::create($attr);
                   // dd($contact);
                    Mail::send('emails.new_contact', array('contact' => $contact), function ($message) use ($contact) {
                        $message->from(config('mail.username'), config('mail.label'));
                        $message->to($contact->email, $contact->name);
                        $message->subject(config('mail.subject_new_contact'));
                    });

                    $attr = (object) $attr;
                    Mail::send('emails.new_contact_us', array('contact' => $contact), function ($message) use ($attr) {
                        $message->from(config('mail.username'), config('mail.label_us'));
                        $message->to(config('mail.username_us'), config('mail.label_us'));
                        $message->subject(sprintf(config('mail.subject_new_contact_us'), $attr->company));
                    });
                });

                //return response()->json(['send' => 'yes']);
              //  return response()->json();

            } catch (Exception $e) {
                return response()->json(['send' => 'Error'], 500);
            }

        } else {
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $hash = $request->input("hashid");
        $client = Contact::find($id);
        // dd($client);
        if ($client == null) {
            abort(404);
        }
        if ($client->hash == $hash) {
            $logos = Portfolio::all();

//            $project = Project::where('contact_id', '=', $client->id)->get()->first();
//            $datatime = Project::find($project->id)->timelines;
            return view('contact/clientpage', ['client' => $client, 'logos' => $logos]);
        } else {
            return redirect('/');
        }
        //
    }

    public function fotos($id)
    {
        $logo = Portfolio::find($id);
        if ($logo != null) {
            if (File::exists('appfiles/portfolio/' . $logo->map)) {
                $files = File::files('appfiles/portfolio/' . $logo->map);

                //dd($files);

                return view('contact.gallery', ['files' => $files]);
            } else {
                abourt(404);
            }
        }
    }

    public function message(Request $request)
    {

        $this->validate($request, [
            'message' => 'required',
        ]);
        ClientMessage::create($request->all());

        //return redirect('/');
        return response()->json(['message' => 'ok']);
    }

    public function confirm(Request $request)
    {
        $user = $request->input('client');
        $this->client = Contact::find($user);
        $this->client->status = 1;
        $this->client->save();
        $sent = Mail::send('emails.confirm', array('id' => $user), function ($message) {
            $message->from($this->client->email);
            $message->to('info@we3uk.com', 'John Smith')->subject('Confirm!');
        });
        //mail
        //return redirect('/');
        return response()->json(['confirm' => 'ok']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    public function showPdfForm(Request $r)
    {
        $id = $r->input('id');
        $con = Contact::find($id);
        if ($con == null) {
            return redirect('/');
        } else {

            if (Pdfstatus::where(['contact_id' => $con->id])->first() == null) {

                return view("contact.clientform", ['id' => $con->id]);
            } else {
                return view("contact.invalid", ['id' => $con->id]);
            }

        }
    }

    public function form(Request $request)
    {
        //  dd($request);


        $this->validate($request, [
            "company" => "required",
            "companyage" => "required",
            "webpage" => "required",
            "industry:" => "required",
            "productoff" => "required",
            "productprice" => "required",
            "companymiss" => "required",
            "disired" => "required",
            "aboutcompany" => "",
            "name" => "",
            "website" => "",
            "pros" => "",
            "cons" => "",
            "howare" => "",
            "location" => "required",
            "demographics" => "",
            "audience" => "required",
            "rezult" => "",
            "income" => "",
            "opinion" => "required",
            "worst" => "required",
            "definitely" => "required",
            "no" => "required",
            "planned" => "required",
            "websitefollow" => "required",
            "websitefollother" => "",
            "functionwebsite" => "required",
            "othermodule" => "",
            "otherlan" => "",
            "planupdate" => "",
            "contentman" => "required",
            "special" => "",
        ]);

        $pdf = PDF::loadView('contact.pdfclient', ['from' => $request->all()]);
        // dd($pdf);

        $out = $pdf->stream();
        $file = $request->input("id");
        $pdf = new Pdfstatus();
        $pdf->contact_id = $request->input("id");
        $pdf->filename = "user$file.pdf";
        $pdf->fill = 1;
        $pdf->save();
        File::put("pdf/user$file.pdf", $out);

        // return view('contact.pdfclient',['from'=>$request->all()]);
        return redirect("/contact/clientform/?id=$file");


        // return  view("contact.clientform");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
