<?php namespace App\Http\Controllers;

use App\PriceOffer;
use App\Portfolio;
use App\Service;
use App\ClientMessage;
use App;
use Exception;
use Mail;
use Redirect;
use Log;
use Illuminate\Http\Request;
use DB;

class PriceOffersController extends Controller
{

    /**
     * @param $hash
     * @return PriceOffer
     */
    protected function priceOffer($hash) {
        $price_offer = PriceOffer::where('hash', $hash)->first();
        if ($price_offer === null || (int)$price_offer->is_viewable !== 1) {
            App::abort(404);
        }
        return $price_offer;
    }

    public function get($hash)
    {
        $price_offer = $this->priceOffer($hash);

        $price_offer->loadServices();
        $price_offer->loadPortfolios();
        $price_offer_services = $price_offer->services;
        $services = Service::all();
        $servicesContainer = array();
        $servicesDataContainer = array();
        foreach ($price_offer_services as $price_offer_service) {
            foreach ($services as $service) {
                if ($price_offer_service->service_id === $service->id) {
                    $servicesContainer[$service->id][] = $price_offer_service;
                    $servicesDataContainer[$service->id] = [$service->title_en, $service->getIconUrl()];
                    break;
                }
            }
        }
        ksort($servicesContainer);
      //  dd($price_offer->getPortfolioIds());
        $portfolios = Portfolio::whereIn('id', $price_offer->getPortfolioIds())->get();

        return view('price_offer', compact('price_offer', 'servicesContainer', 'servicesDataContainer', 'portfolios'));
    }

    public function postConfirm($hash)
    {
        $price_offer = $this->priceOffer($hash);

        if ($price_offer->contact->price_offer_status === 2) {
//            return Redirect::back()->with(
//                [
//                    'message' => 'This Price Offer was confirmed.',
//                    'message-type' => 'danger'
//                ]
//            );
            return response()->json([ 'message' => '2']);
        }

        try {
            Mail::send(
                'emails.price_offer_confirmed_us',
                array('price_offer' => $price_offer),
                function ($message) use ($price_offer) {
                    $message->from(config('mail.username'), config('mail.label_us'));
                    $message->to(config('mail.username_us'), config('mail.label_us'));
                    $message->subject(
                        sprintf(config('mail.subject_price_offer_confirmed_us'), $price_offer->contact->company)
                    );
                }
            );

            Mail::send(
                'emails.price_offer_confirmed',
                array('price_offer' => $price_offer),
                function ($message) use ($price_offer) {
                    $message->from(config('mail.username'), config('mail.label'));
                    $message->to($price_offer->contact->email, $price_offer->contact->name);
                    $message->subject(sprintf(
                        config('mail.subject_price_offer_confirmed'),
                        $price_offer->contact->company
                    ));
                }
            );

            $price_offer->contact->price_offer_status = 2;
            $price_offer->contact->save();

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return Redirect::back()->with(
                [
                    'message' => 'There was an error. Please try again later.',
                    'message-type' => 'danger'
                ]
            );

        }

      //  return redirect('/')->with(['message' => 'Thank you for confirming Price Offer']);
        return response()->json([ 'message' => '1']);
    }

    /**
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postConfirm2($hash)
    {
        $price_offer = $this->priceOffer($hash);
        if ($price_offer->price_offer_status === 2) {

            return response()->json([ 'message' => '2']);
        }

        try {
            Mail::send(
                'emails.price_offer_confirmed_us',
                array('price_offer' => $price_offer),
                function ($message) use ($price_offer) {
                    $message->from(config('mail.username'), config('mail.label_us'));
                    $message->to(config('mail.username_us'), config('mail.label_us'));
                    $message->subject(
                        sprintf(config('mail.subject_price_offer_confirmed_us'), $price_offer->company_name)
                    );
                }
            );

            Mail::send(
                'emails.price_offer_confirmed',
                array('price_offer' => $price_offer),
                function ($message) use ($price_offer) {
                    $message->from(config('mail.username'), config('mail.label'));
                    $message->to($price_offer->contact_email, $price_offer->contact_person_name);
                    $message->subject(sprintf(
                        config('mail.subject_price_offer_confirmed'),
                        $price_offer->company_name
                    ));
                }
            );

            $price_offer->price_offer_status = 2;
            $price_offer->save();

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return Redirect::back()->with(
                [
                    'message' => 'There was an error. Please try again later.',
                    'message-type' => 'danger'
                ]
            );

        }

        return response()->json([ 'message' => '1']);
    }


    public function postSendMessage(Request $request, $hash)
    {
        $price_offer = $this->priceOffer($hash);

        try {
            DB::transaction(function () use ($price_offer, $request) {

                Mail::send(
                    'emails.price_offer_rejected_us',
                    array('price_offer' => $price_offer),
                    function ($message) use ($price_offer) {
                        $message->from(config('mail.username'), config('mail.label_us'));
                        $message->to(config('mail.username_us'), config('mail.label_us'));
                        $message->subject(
                            sprintf(config('mail.subject_price_offer_rejected_us'), $price_offer->contact->company)
                        );
                    }
                );

                Mail::send(
                    'emails.price_offer_rejected',
                    array('price_offer' => $price_offer),
                    function ($message) use ($price_offer) {
                        $message->from(config('mail.username'), config('mail.label'));
                        $message->to($price_offer->contact->email, $price_offer->contact->name);
                        $message->subject(sprintf(
                            config('mail.subject_price_offer_rejected'),
                            $price_offer->contact->company
                        ));
                    }
                );

                $price_offer->contact->complaint_message = $request->input('message');
                $price_offer->contact->price_offer_status = 3;
                $price_offer->contact->save();

            });

        } catch (Exception $e) {
            Log::error($e->getMessage());
//            return Redirect::back()->with(
//                [
//                    'message' => 'There was an error. Please try again later.',
//                    'message-type' => 'danger'
//                ]
//            );
            return response()->json([ 'message' => 'There was an error. Please try again later.']);
        }
//
//        return Redirect::back()->with(
//            [
//                'message' => 'Thank you for sending us a message. We will check it soon and fix your Price Offer.'
//            ]
//        );
        return response()->json([ 'message' => '1']);
    }

    public function postSendMessages(Request $request, $hash)
    {
        $price_offer = $this->priceOffer($hash);

        try {
            DB::transaction(function () use ($price_offer, $request) {

                Mail::send(
                    'emails.price_offer_rejected_us2',
                    array('price_offer' => $price_offer),
                    function ($message) use ($price_offer) {
                        $message->from(config('mail.username'), config('mail.label_us'));
                        $message->to(config('mail.username_us'), config('mail.label_us'));
                        $message->subject(
                            sprintf(config('mail.subject_price_offer_rejected_us'), $price_offer->company_name)
                        );
                    }
                );

                Mail::send(
                    'emails.price_offer_rejected',
                    array('price_offer' => $price_offer),
                    function ($message) use ($price_offer) {
                        $message->from(config('mail.username'), config('mail.label'));
                        $message->to($price_offer->contact_email, $price_offer->contact_person_name);
                        $message->subject(sprintf(
                            config('mail.subject_price_offer_rejected'),
                            $price_offer->company_name
                        ));
                    }
                );


                $price_offer->price_offer_status = 3;

                $price_offer->save();
                $clientMessage = new ClientMessage();
                $clientMessage->contact_id =0;
                $clientMessage->message = $request->input('message');
                $clientMessage->price_id = $request->input('price_message');
                $clientMessage->save();
            });

        } catch (Exception $e) {
            Log::error($e->getMessage());
//            return Redirect::back()->with(
//                [
//                    'message' => 'There was an error. Please try again later.',
//                    'message-type' => 'danger'
//                ]
//            );
            return response()->json([ 'message' => 'There was an error. Please try again later.']);
        }
//
//        return Redirect::back()->with(
//            [
//                'message' => 'Thank you for sending us a message. We will check it soon and fix your Price Offer.'
//            ]
//        );
       // return response()->json([ 'message' => '1']);
    }

}
