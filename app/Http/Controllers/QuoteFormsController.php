<?php namespace App\Http\Controllers;

use App\QuoteForm;
use App;
use DB;
use Exception;
use File;
use Illuminate\Http\Request;
use PDF;
use Redirect;
use Mail;
use Response;

class QuoteFormsController extends Controller
{

    public function get($hash)
    {
        $quote_form = QuoteForm::where('hash', $hash)->first();
        if ($quote_form === null || (int)$quote_form->is_filled === 1) {
            App::abort(404);
        }

        /*if ((int)$quote_form->is_filled === 1) {
            $headers = array(
                'Content-Type: application/pdf',
            );
            return Response::download($quote_form->getPdfPath(), 'WE3_quote_form_'.$quote_form->hash.'.pdf', $headers);
        } else {*/
            return view('quote_form');
        //}
    }

    public function post(Request $request, $hash)
    {
        $quote_form = QuoteForm::where('hash', $hash)->first();
        if ($quote_form === null || (int)$quote_form->is_filled === 1) {
            App::abort(404);
        }

        // 1. Validate form
        $this->validate($request, [
            "company" => "required",
            "companyage" => "required",
            "webpage" => "required",
            "industry:" => "required",
            "productoff" => "required",
            "productprice" => "required",
            "companymiss" => "required",
            "disired" => "required",
            "aboutcompany" => "",
            "name" => "",
            "website" => "",
            "pros" => "",
            "cons" => "",
            "howare" => "",
            "location" => "required",
            "demographics" => "",
            "audience" => "required",

            "rezult" => "",
            "income" => "",
            "opinion" => "required",
            "worst" => "required",
            "definitely" => "required",
            "no" => "required",
            "planned" => "required",
            "websitefollow" => "required",
            "websitefollother" => "",
            "functionwebsite" => "required",
            "othermodule" => "",
            "otherlan" => "",
            "planupdate" => "",
            "contentman" => "required",
            "special" => "",
        ]);

        try {
            DB::transaction(function () use ($quote_form, $request) {
                // 2. Create PDF file
                $pdf = PDF::loadView('contact.pdfclient', ['from' => $request->all()]);
                $out = $pdf->stream();
                if (File::isDirectory('appfiles/quote_forms/') === false) {
                    $dir_made = File::makeDirectory('appfiles/quote_forms/');
                    if (!$dir_made) {
                        throw new Exception('Error creating new directory');
                    }
                }

                $dir_made = File::makeDirectory('appfiles/quote_forms/'.$quote_form->contact_id.'/');
                if (!$dir_made) {
                    throw new Exception('Error creating new directory');
                }
                $file_saved = File::put('appfiles/quote_forms/'.
                    $quote_form->contact_id.'/'.$quote_form->hash.'.pdf', $out);
                if (!$file_saved) {
                    throw new Exception('Error creating PDF document');
                }

                // 3. Set is_filled 1 at QuoteForm model
                $quote_form->is_filled = 1;
                $saved = $quote_form->save();
                if (!$saved) {
                    throw new Exception('Error updating QuoteForm model');
                }

                // 4. Set status=2 (Quote form filled) at Contact model
                $quote_form->contact->status = 2;
                $contact_saved = $quote_form->contact->save();
                if (!$contact_saved) {
                    throw new Exception('Error updating Contact model');
                }

                // 4. Send Email to client
                $client_mail_sent = Mail::send(
                    'emails.quote_form_filled',
                    array('name' => $quote_form->contact->name, 'quote_form' => $quote_form),
                    function ($message) use ($quote_form) {
                        $message->from(config('mail.username'), config('mail.label'));
                        $message->to($quote_form->contact->email, $quote_form->contact->name);
                        $message->subject(sprintf(
                            config('mail.subject_quote_form_filled'),
                            $quote_form->contact->company
                        ));
                    }
                );
                if (!$client_mail_sent) {
                    throw new Exception('Error sending email to client '.$quote_form->contact->email);
                }
            });
        } catch (Exception $e) {
            if (File::exists('appfiles/quote_forms/'.$quote_form->contact_id.'/'.$quote_form->hash.'.pdf')) {
                File::deleteDirectory('appfiles/quote_forms/'.$quote_form->contact_id.'/');
            }

            return Redirect::back()->with(
                [
                    'message' => $e->getMessage(),
                    'message-type' => 'danger'
                ]
            );
        }

        // 5. Send email to us
        Mail::send(
            'emails.quote_form_filled_us',
            array('quote_form' => $quote_form),
            function ($message) use ($quote_form) {
                $message->from(config('mail.username'), config('mail.label_us'));
                $message->to(config('mail.username_us'), config('mail.label_us'));
                $message->subject(sprintf(config('mail.subject_quote_form_filled_us'), $quote_form->contact->company));
            }
        );

        // return view('contact.pdfclient',['from'=>$request->all()]);
        return redirect('/')->with(['message' => 'Thank you for filling Quote Form']);
    }

    public function getPdf($hash)
    {

    }

}
