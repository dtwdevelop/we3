<?php namespace App\Http\Middleware;

use Closure;

class Headers
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        header_remove('X-Powered-By');
        header_remove('Server');

        $headers = [
            'Server' => 'Microsoft Azure/1.5',
            'X-AspNetMvc-Version' => '3.0',
            'X-AspNet-Version' => '4.0.30319',
            'X-Powered-By' => 'ASP.NET'
        ];

        $response = $next($request);
        foreach ($headers as $key => $value) {
            $response->headers->set($key, $value, true);
        }

        return $response;
    }

}
