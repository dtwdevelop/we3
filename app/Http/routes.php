<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

//Route::get('home', 'HomeController@index');
//Route::get('about', 'PagesController@about');
//Route::get('contact', 'PagesController@contact');

Route::pattern('id', '[0-9]+');
//Route::get('news/{id}', 'ArticlesController@show');
//Route::get('video/{id}', 'VideoController@show');
//Route::get('photo/{id}', 'PhotoController@show');
//Route::get('/contact', 'ContactController@create');
Route::post('/contact/send', 'ContactController@store');


//clientpage
//Route::get('/clientpage/fotos/{id}', 'ContactController@fotos');
//Route::get('/clientpage/{id}', 'ContactController@show');
//Route::post('/contact/message', 'ContactController@message');
//Route::post('/contact/confirm', 'ContactController@confirm');

# Quote Form
Route::get('/quote_form/{hash}', 'QuoteFormsController@get');
Route::post('/quote_form/{hash}', 'QuoteFormsController@post');

# Price offer
Route::get('price_offer/{hash}', 'PriceOffersController@get');
Route::post('price_offer/{hash}/confirm', 'PriceOffersController@postConfirm');
Route::post('price_offer/{hash}/confirm2', 'PriceOffersController@postConfirm2');
Route::post('price_offer/{hash}/send-message', 'PriceOffersController@postSendMessage');
Route::post('price_offer/{hash}/send-messages', 'PriceOffersController@postSendMessages');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin'], function () {
    Route::pattern('id', '[0-9]+');
    Route::pattern('id2', '[0-9]+');

    # Admin Dashboard
    Route::get('dashboard', 'DashboardController@index');
    Route::get('/', 'DashboardController@index');

    # Language
    /*Route::get('language', 'LanguageController@index');
    Route::get('language/create', 'LanguageController@getCreate');
    Route::post('language/create', 'LanguageController@postCreate');
    Route::get('language/{id}/edit', 'LanguageController@getEdit');
    Route::post('language/{id}/edit', 'LanguageController@postEdit');
    Route::get('language/{id}/delete', 'LanguageController@getDelete');
    Route::post('language/{id}/delete', 'LanguageController@postDelete');
    Route::get('language/data', 'LanguageController@data');
    Route::get('language/reorder', 'LanguageController@getReorder');*/

    # News category
    /*Route::get('newscategory', 'ArticleCategoriesController@index');
    Route::get('newscategory/create', 'ArticleCategoriesController@getCreate');
    Route::post('newscategory/create', 'ArticleCategoriesController@postCreate');
    Route::get('newscategory/{id}/edit', 'ArticleCategoriesController@getEdit');
    Route::post('newscategory/{id}/edit', 'ArticleCategoriesController@postEdit');
    Route::get('newscategory/{id}/delete', 'ArticleCategoriesController@getDelete');
    Route::post('newscategory/{id}/delete', 'ArticleCategoriesController@postDelete');
    Route::get('newscategory/data', 'ArticleCategoriesController@data');
    Route::get('newscategory/reorder', 'ArticleCategoriesController@getReorder');/

    # News
    /*Route::get('news', 'ArticlesController@index');
    Route::get('news/create', 'ArticlesController@getCreate');
    Route::post('news/create', 'ArticlesController@postCreate');
    Route::get('news/{id}/edit', 'ArticlesController@getEdit');
    Route::post('news/{id}/edit', 'ArticlesController@postEdit');
    Route::get('news/{id}/delete', 'ArticlesController@getDelete');
    Route::post('news/{id}/delete', 'ArticlesController@postDelete');
    Route::get('news/data', 'ArticlesController@data');
    Route::get('news/reorder', 'ArticlesController@getReorder');*/

    # Photo Album
    /*Route::get('photoalbum', 'PhotoAlbumController@index');
    Route::get('photoalbum/create', 'PhotoAlbumController@getCreate');
    Route::post('photoalbum/create', 'PhotoAlbumController@postCreate');
    Route::get('photoalbum/{id}/edit', 'PhotoAlbumController@getEdit');
    Route::post('photoalbum/{id}/edit', 'PhotoAlbumController@postEdit');
    Route::get('photoalbum/{id}/delete', 'PhotoAlbumController@getDelete');
    Route::post('photoalbum/{id}/delete', 'PhotoAlbumController@postDelete');
    Route::get('photoalbum/data', 'PhotoAlbumController@data');
    Route::get('photoalbum/reorder', 'PhotoAlbumController@getReorder');*/

    # Photo
    /*Route::get('photo', 'PhotoController@index');
    Route::get('photo/create', 'PhotoController@getCreate');
    Route::post('photo/create', 'PhotoController@postCreate');
    Route::get('photo/{id}/edit', 'PhotoController@getEdit');
    Route::post('photo/{id}/edit', 'PhotoController@postEdit');
    Route::get('photo/{id}/delete', 'PhotoController@getDelete');
    Route::post('photo/{id}/delete', 'PhotoController@postDelete');
    Route::get('photo/{id}/itemsforalbum', 'PhotoController@itemsForAlbum');
    Route::get('photo/{id}/{id2}/slider', 'PhotoController@getSlider');
    Route::get('photo/{id}/{id2}/albumcover', 'PhotoController@getAlbumCover');
    Route::get('photo/data/{id}', 'PhotoController@data');
    Route::get('photo/reorder', 'PhotoController@getReorder');*/

    # Video
    /*Route::get('videoalbum', 'VideoAlbumController@index');
    Route::get('videoalbum/create', 'VideoAlbumController@getCreate');
    Route::post('videoalbum/create', 'VideoAlbumController@postCreate');
    Route::get('videoalbum/{id}/edit', 'VideoAlbumController@getEdit');
    Route::post('videoalbum/{id}/edit', 'VideoAlbumController@postEdit');
    Route::get('videoalbum/{id}/delete', 'VideoAlbumController@getDelete');
    Route::post('videoalbum/{id}/delete', 'VideoAlbumController@postDelete');
    Route::get('videoalbum/data', 'VideoAlbumController@data');
    Route::get('video/reorder', 'VideoAlbumController@getReorder');*/

    # Users
    Route::get('users/', 'UserController@index');
    Route::get('users/create', 'UserController@getCreate');
    Route::post('users/create', 'UserController@postCreate');
    Route::get('users/{id}/edit', 'UserController@getEdit');
    Route::post('users/{id}/edit', 'UserController@postEdit');
    Route::get('users/{id}/delete', 'UserController@getDelete');
    Route::post('users/{id}/delete', 'UserController@postDelete');
    Route::get('users/data', 'UserController@data');

    # Services
    Route::get('services/', 'ServicesController@index');
    Route::get('services/create', 'ServicesController@getCreate');
    Route::post('services/create', 'ServicesController@postCreate');
    Route::get('services/{id}/edit', 'ServicesController@getEdit');
    Route::post('services/{id}/edit', 'ServicesController@postEdit');
    Route::get('services/{id}/delete', 'ServicesController@getDelete');

    # Sliders
    Route::get('sliders/', 'SlidersController@index');
    Route::get('sliders/create', 'SlidersController@getCreate');
    Route::post('sliders/create', 'SlidersController@postCreate');
    Route::get('sliders/{id}/edit', 'SlidersController@getEdit');
    Route::post('sliders/{id}/edit', 'SlidersController@postEdit');
    Route::get('sliders/{id}/delete', 'SlidersController@getDelete');

    # Portfolio
    Route::get('portfolio/', 'PortfolioController@index');
    Route::get('portfolio/add', 'PortfolioController@create');
    Route::post('portfolio/create', 'PortfolioController@store');
    Route::post('portfolio/fotoupload', 'PortfolioController@uploadfoto');
    Route::post('portfolio/update/{id}', 'PortfolioController@update');
    Route::get('portfolio/edit/{id}', 'PortfolioController@edit');
    Route::get('portfolio/fotolists/{id}', 'PortfolioController@fotolist');
    Route::get('portfolio/showlogo/{id}', 'PortfolioController@showfoto');
    Route::delete('portfolio/destroy/{id}', 'PortfolioController@destroy');
    Route::get('portfolio/fotodelete/{id}', 'PortfolioController@fotodelete');

    # Contact
    Route::get('contact/', 'ContactController@index');
    Route::get('contact/{id}', 'ContactController@get');
    //Route::get('contact/config', 'ContactController@config');
    //Route::post('contact/config', 'ContactController@config');
    Route::get('contact/{id}/delete', 'ContactController@getDelete');
    //Route::get('contact/sendform/{id}', 'ContactController@sendForm');
    //Route::get('contact/sendconfirm/{id}', 'ContactController@sendConfirmation');
    Route::get('contact/send-quote-form/{id}', 'ContactController@getSendQuoteForm');
    Route::get('contact/resend-quote-form/{id}', 'ContactController@getResendQuoteForm');
    Route::get('contact/view-complaint/{id}', 'ContactController@getViewComplaint');

    # Message
    Route::get('message/{id}', 'MessagesController@show');
    Route::get('messages/{id}', 'MessagesController@show2');

    # Configurations
    Route::get('config/', 'ConfigController@index');
    Route::post('config/', 'ConfigController@postUpdate');

    # Price offers
    Route::get('price_offers/', 'PriceOffersController@index');
    Route::get('price_offers/create/', 'PriceOffersController@getCreatePrice');
    Route::post('price_offers/send/{contact_id}', 'PriceOffersController@postCreate2');
    Route::get('price_offers/edit/{id}', 'PriceOffersController@getEdit2');
    Route::post('price_offers/edit/{id}', 'PriceOffersController@postEdit2');

    Route::get('price_offers/create/{contact_id}', 'PriceOffersController@getCreate');
    Route::post('price_offers/create/{contact_id}', 'PriceOffersController@postCreate');
   //
    Route::get('price_offers/{id}/edit', 'PriceOffersController@getEdit');
    Route::post('price_offers/{id}/edit', 'PriceOffersController@postEdit');
    //Route::get('price_offers/delete/{contact_id}', 'PriceOffersController@getDelete');
    Route::get('price_offers/{contact_id}/send', 'PriceOffersController@getSend');
    Route::get('price_offers/send/{contact_id}', 'PriceOffersController@getSend2');
    Route::get('price_offers/{contact_id}/resend', 'PriceOffersController@getResend');

});
