<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

	//
  public function timelines(){
       return $this->hasMany("App\Timeline","project_id");
    }
}
