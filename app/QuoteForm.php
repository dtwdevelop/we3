<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use URL;

class QuoteForm extends Model
{
    protected $fillable = array();

    protected $table = 'quote_forms';

    protected $primaryKey = 'id';

    public function isFilled()
    {
        return $this->is_filled;
    }

    public function getPdfUrl()
    {
        return URL::to('/appfiles/quote_forms/' . $this->contact_id . '/' . $this->hash . '.pdf');
    }

    public function getModuleDir()
    {
        return public_path() . '/appfiles/quote_forms/';
    }

    public function getPdfDir()
    {
        return public_path() . '/appfiles/quote_forms/' . $this->contact_id . '/';
    }

    public function getPdfPath()
    {
        return public_path() . '/appfiles/quote_forms/' . $this->contact_id . '/' . $this->hash . '.pdf';
    }

    public function contact()
    {
        return $this->belongsTo("App\Contact", 'contact_id');
    }

}
