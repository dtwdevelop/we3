<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Portfolios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
Schema::create('porfolio', function($table)
{
       $table->increments('id')->unsigned();
       $table->string('title');
       $table->text('description');
       $table->string('map');
       $table->string('logo');
       $table->string('url');
       $table->boolean('hide');
       $table->timestamps();
});//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('porfolio');
	}

}
