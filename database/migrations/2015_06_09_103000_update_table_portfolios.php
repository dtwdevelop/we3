<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTablePortfolios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::drop('portfolios');
        Schema::rename('porfolio', 'portfolios');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::rename('portfolios', 'porfolio');

        Schema::create('portfolios', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
        });
	}

}
