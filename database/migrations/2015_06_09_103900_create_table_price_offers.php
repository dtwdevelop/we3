<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePriceOffers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        Schema::create('price_offers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->char('hash', 40)->unique();
            $table->unsignedInteger('contact_id');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->char('doc_nr', 128);
            $table->char('company_name', 128);
            $table->char('contact_person_name', 128);
            $table->char('contact_email', 128);
            $table->char('contact_phone', 32);
            $table->tinyInteger('is_viewable');
            $table->tinyInteger('is_sent');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->unsignedInteger('user_id_edited')->nullable();
            $table->foreign('user_id_edited')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('price_offer_services', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->unsignedInteger('price_offer_id');
            $table->unsignedInteger('service_id');
            $table->char('label', 64);
            $table->dateTime('datetime_from');
            $table->dateTime('datetime_to');
            $table->decimal('price', 8, 2);
            $table->foreign('price_offer_id')->references('id')->on('price_offers')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->unsignedInteger('user_id_edited')->nullable();
            $table->foreign('user_id_edited')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('price_offer_portfolios', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->unsignedInteger('price_offer_id');
            $table->unsignedInteger('portfolio_id');
            $table->foreign('price_offer_id')->references('id')->on('price_offers')->onDelete('cascade');
            $table->foreign('portfolio_id')->references('id')->on('portfolios')->onDelete('cascade');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->unsignedInteger('user_id_edited')->nullable();
            $table->foreign('user_id_edited')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });

        Schema::table('contacts', function($table) {
            $table->unsignedInteger('price_offer_id')->nullable();
            $table->foreign('price_offer_id')->references('id')->on('price_offers')->onDelete('set null');
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

        Schema::table('contacts', function($table) {
            $table->dropForeign('contacts_price_offer_id_foreign');
            $table->dropColumn('price_offer_id');
        });

        Schema::drop('price_offer_portfolios');
        Schema::drop('price_offer_services');
        Schema::drop('price_offers');
	}

}
