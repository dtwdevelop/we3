<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStatusesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_statuses', function(Blueprint $table)
		{
            $table->increments('id')->unsigned();
            $table->char('name_en', 32);
            $table->char('name_ru', 32);
            $table->char('name_lv', 32);
		});

        Schema::table('price_offers', function($table) {
            $table->unsignedInteger('status_id')->nullable();
            $table->foreign('status_id')->references('id')->on('order_statuses')->onDelete('set null');
        });

        DB::table('order_statuses')->insert(array(
            'name_en' => 'New',
            'name_lv' => 'New',
            'name_ru' => 'New',
        ));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('price_offers', function($table) {
            $table->dropForeign('price_offers_status_id_foreign');
            $table->dropColumn('status_id');
        });

        Schema::drop('order_statuses');
	}

}
