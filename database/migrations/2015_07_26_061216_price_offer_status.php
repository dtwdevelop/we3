<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PriceOfferStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('price_offers', function($table) {

			if(!Schema::hasColumn('price_offers','price_offer_status')){
				$table->integer('price_offer_status')->nullable();
			}


		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('price_offers', function($table) {

			if(Schema::hasColumn('price_offers','price_offer_status')){
				$table->dropColumn('price_offer_status');
			}


		});
	}

}
