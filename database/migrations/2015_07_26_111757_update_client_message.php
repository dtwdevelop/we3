<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClientMessage extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_messages', function($table) {

			if(!Schema::hasColumn('client_messages','price_offer_status')){
				$table->integer('price_id')->nullable();
			}


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('client_messages', function($table) {

			if(!Schema::hasColumn('client_messages','price_offer_status')){
				$table->dropColumn('price_id');
			}


		});
	}

}
