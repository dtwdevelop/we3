/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
  // When the window has finished loading create our google map below
                google.maps.event.addDomListener(window, 'load', init);

                function init() {
                    // Basic options for a simple Google Map
                    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                    var mapOptions = {
                        // How zoomed in you want the map to start at (always required)
                        zoom: 12,
                        // The latitude and longitude to center the map (always required)
                        center: new google.maps.LatLng(51.5075432, -0.1276483), // New York

                        // How you would like to style the map. 
                        // This is where you would paste any style found on Snazzy Maps.
                        styles: [{"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]}, {"featureType": "landscape", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]}, {"featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{"color": "#ffffff"}, {"lightness": 17}]}, {"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]}, {"featureType": "road.arterial", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 18}]}, {"featureType": "road.local", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 16}]}, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]}, {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#dedede"}, {"lightness": 21}]}, {"elementType": "labels.text.stroke", "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]}, {"elementType": "labels.text.fill", "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]}, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "transit", "elementType": "geometry", "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]}, {"featureType": "administrative", "elementType": "geometry.fill", "stylers": [{"color": "#fefefe"}, {"lightness": 20}]}, {"featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]}]
                    };

                    // Get the HTML DOM element that will contain your map 
                    // We are using a div with id="map" seen below in the <body>
                    var mapElement = document.getElementById('map');

                    // Create the Google Map using our element and options defined above
                    var map = new google.maps.Map(mapElement, mapOptions);

                    // Let's also add a marker while we're at it
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(40.6700, -73.9400),
                        map: map,
                        title: 'Snazzy!'
                    });
                }




   $(document).ready(function () {
                    
                     //send confirm
                    jQuery(".confirm").click(function () {
                         var input = $(".formc").serialize();
                         $.post("/contact/confirm", input, function (data) {
                            $(".errorf").hide();
                             $('.popmod2').modal('show');
//                            $(".formc").hide();

                        }, 'json');
                         $('#popmod2').modal('show');
                         
                         
                    });

                    jQuery(".field").focus(function () {
                        jQuery(".confirm").attr("disabled","disabled");
                      
                       jQuery(".btnc").removeAttr('disabled');
                       
                    
                    });
                    
                     jQuery(".field").mouseleave(function () {
                         jQuery(".confirm").removeAttr('disabled');
                          jQuery(".btnc").removeAttr('disabled');
                    
                        
                     });
                    
                    jQuery(".confirm").mouseover(function(){
                         jQuery(".btnc").attr("disabled","disabled");
                          jQuery(".confirm").removeAttr('disabled');
                    });
                    
                   
                        
                      
                        
                    

                    var el = $(":input[name='message']")


                    el.keypress(function () {

                        if (el.val() != " ") {

                            el.attr({"style": "background-color:white;color:green"});

                        }

                    });









//                    send message
                    $('.btnc').click(function (e) {

                        e.preventDefault();
                        var input = $(".formc2").serialize();




                        el.keypress(function () {

                            if (el.val() != " ") {

                                el.attr({"style": "background-color:white;color:green"});

                            }

                        });


                        $.post("/contact/message", input, function (data) {
                            $(".errorf").hide();
                             $('#popmod').modal('show');
//                           

                        }, 'json').fail(function (data) {


                            $.each(data.responseJSON, function (i, v) {

                                if (i == "message") {
                                    var textar = $("textarea[name='" + i + "']").attr({"style": "background-color:red;color:white;"});

                                    textar.attr('placeholder', v);
                                }






                            });

                        });

                    });
                    
                     $(".cls").click(function(e){
                          e.preventDefault();
                          var el =  $(this).attr('href');
                          jQuery(el).animatescroll({scrollSpeed:2000,padding:1});
//                     
                    
                   });
                   
              
    

                });