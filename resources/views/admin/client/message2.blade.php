@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {{{ trans("admin/photo.photo") }}} @parent @stop

{{-- Content --}}
@section('main')
 <div class="container">


<div class="row">
    <div class="col-md-6">
   
    <a class="btn btn-success" href="{{ url("/admin/price_offers")}}">Back</a>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

                <dl class="dl-horizontal">
                    <dt>Company name:</dt>
                    <dd>{{$contact->company_name}}</dd>
                    <dt>Contact person name:</dt>
                    <dd>{{$contact->contact_person_name}}</dd>
                    <dt>Company email:</dt>
                    <dd>{{$contact->contact_email}}</dd>
                    <dt>Company phone:</dt>
                    <dd>{{$contact->contact_phone}}</dd>




                </dl>
                <dd>
                    <a href="{{URL::to('/admin/price_offers/edit/'.$contact->id)}}">
                        {{$contact->doc_nr}}
                        <button class="btn btn-xs btn-primary">Edit</button>
                    </a>
                </dd>

            </div>
        </div>
</div>
</div>
<h3>Messages</h3>
@foreach($messages as $msg)

<div class="row">
    <div class="col-lg-6">{{$msg->message}}</div>
</div>
<hr>
@endforeach

</div>
@stop

{{-- Scripts --}}
@section('scripts')
    @parent
 
@stop