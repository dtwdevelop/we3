@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {{{ trans("admin/photo.photo") }}} @parent @stop

{{-- Content --}}
@section('main')


    <div class="page-header">
        <div class="row">
            <div class="col-lg-7">
                 @if (count($errors) > 0)
	<div class="alert alert-danger">
	<strong>Whoops!</strong> There were some problems with your input.<br><br>
	<ul>
	@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
           </ul>
</div>
@endif
 {!! Form::model($form,['url'=>'/admin/contact/config']) !!}
 {!!Form::label('phone','Phone') !!}
 {!!Form::text('phone',null,['class'=>'form-control']) !!}
 
  {!!Form::label('mails','e-Mail:') !!}
 {!!Form::text('mails',null,['class'=>'form-control']) !!}
 
 
 {!!Form::label('adress','Address:') !!}
 {!!Form::text('adress',null,['class'=>'form-control']) !!}

 {!! Form::submit("Create",['class'=>'btn  btn-danger'])!!}
 {!! Form::close() !!}
            </div>
        </div>
    </div>

    
@stop

{{-- Scripts --}}
@section('scripts')
    @parent
 
@stop


