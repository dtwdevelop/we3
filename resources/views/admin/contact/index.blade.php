<?php
use Session;
?>
@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {{{ trans("admin/photo.photo") }}} @parent @stop

{{-- Content --}}
@section('main')

@if(Session::has('pdf'))
<p class="alert alert-info">{{ Session::get('pdf') }}</p>
@endif
send
    <div class="page-header">
       
            <h3>{{$title}}</h3>
            
            <div class="container" style="width: 95%;">

              @foreach($contacts as $contact)
              <div class="row">
                  <div class="col-lg-2">
                   Client id: {{$contact->id}}
                   </div>
                  <div class="col-lg-2">
                   Company: {{$contact->company}}
                   </div>
                   <div class="col-lg-2">
                    Name:{{$contact->name}}
                   </div>
                  <div class="col-lg-2">
                    Email:{{$contact->email}}
                   </div>
                  <div class="col-lg-2">
                    Phone:{{$contact->phone}}
                   </div>
                 
                   <div class="col-lg-1">
                    Status:<?php if($contact->status ==0): ?>
                    <i class="">Deactive</ i>
                    <?php else: ?>
                    <i class="">Active</ i>
                     <?php endif; ?>
                   </div>
              </div>
             
        <a class="btn btn-primary" data-toggle="collapse" href="#id{{$contact->id}}" aria-expanded="false" aria-controls="collapseExample">
  Message 
</a>

 <a class="btn btn-warning"  href="/pdf/user{{$contact->id}}.pdf" >
  Show Pdf Form
</a>
 <a class="btn btn-success"  href="/admin/contact/sendform/{{$contact->id}}" >
  Send Pdf Form to mail
</a>
                @if ($contact->status > 0)
                        <a class="btn btn-warning"  href="/admin/price_offers/{{$contact->price_offer_id}}/edit" >
                            Edit Price Offer
                        </a>
                @else
                    <a class="btn btn-success"  href="/admin/price_offers/create/{{$contact->id}}" >
                        Create Price Offer
                    </a>
                @endif
             
              <div class="collapse" id="id{{$contact->id}}">
  <div class="well">
    {{$contact->message}}
  </div>
</div>
              
              @endforeach
             
            </div>
         <?= $contacts->appends(['sort' => 'title'])->render(); ?>
    </div>

    
@stop

{{-- Scripts --}}
@section('scripts')
    @parent
 
@stop


