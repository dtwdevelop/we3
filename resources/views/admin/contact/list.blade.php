@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {{{ trans("admin/admin.messages") }}} - @parent @stop

{{-- Content --}}
@section('main')

    <div class="page-header">
        <h3>
            <a href="{{URL::to('admin/')}}">{{trans('admin/admin.admin_panel')}}</a> >
            @if (Request::get('search') === null)
                {{{ trans("admin/admin.messages") }}}
            @else
                <a href="{{URL::to('/admin/contact')}}">Messages</a> > Filter
            @endif
        </h3>
    </div>

    @if(Session::has('message'))
        <?php $type = (Session::has('message-type')) ? Session::get('message-type') : 'success'; ?>
        <div class="alert alert-{{$type}}" role="alert">
            {{ Session::get('message') }}
        </div>
    @endif

@if(Session::has('pdf'))
<p class="alert alert-success">{{Session::get('pdf') }}</p>
@endif
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        {{--<a href="/admin/price_offers_client" class="btn btn-success">Create price offer to client</a>--}}
        {!! $filter !!}  
        {!! $grid !!}
    </div>
</div>
@stop

@section('styles')
{{!! Rapyd::styles() !!}}
<!--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">-->
@endsection
{{-- Scripts --}}
@section('scripts')
<!--<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
    @parent
 {!! Rapyd::head() !!} 
@stop

