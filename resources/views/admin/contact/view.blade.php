@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Complaint #{{$contact->id}} - @parent @stop

{{-- Content --}}
@section('main')

    <div class="page-header">
        <h3>
            <a href="{{URL::to('admin/')}}">{{trans('admin/admin.admin_panel')}}</a> >
            <a href="{{URL::to('admin/contact')}}">{{ trans("admin/admin.messages") }}</a> >
            Message #{{$contact->id}}
        </h3>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <dl class="dl-horizontal">
                <dt>ID:</dt>
                <dd><a href="{{URL::to('/admin/contact?id='.$contact->id.'&search=1')}}">{{$contact->id}}</a></dd>
                <dt>Date:</dt>
                <dd>{{$contact->created_at->format(config('app.date_format'))}}</dd>
                <dt>Company name:</dt>
                <dd>{{$contact->company}}</dd>
                <dt>Contact person name:</dt>
                <dd>{{$contact->name}}</dd>
                <dt>Company email:</dt>
                <dd>{{$contact->email}}</dd>
                <dt>Company phone:</dt>
                <dd>{{$contact->phone}}</dd>
                <dt>Message:</dt>
                <dd><blockquote>{!!nl2br(e($contact->message))!!}</blockquote></dd>
            </dl>

        </div>
    </div>
@stop
