@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Complaint #{{$contact->id}} - @parent @stop

{{-- Content --}}
@section('main')

    <div class="page-header">
        <h3>
            <a href="{{URL::to('admin/')}}">{{trans('admin/admin.admin_panel')}}</a> >
            <a href="{{URL::to('admin/contact')}}">{{ trans("admin/admin.messages") }}</a> >
            <a href="{{URL::to('admin/contact/'.$contact->id)}}">Message ID {{$contact->id}}</a> >
            Complaint message
        </h3>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <dl class="dl-horizontal">
                <dt>Company name:</dt>
                <dd>{{$contact->priceOffer->company_name}}</dd>
                <dt>Contact person name:</dt>
                <dd>{{$contact->priceOffer->contact_person_name}}</dd>
                <dt>Company email:</dt>
                <dd>{{$contact->priceOffer->contact_email}}</dd>
                <dt>Company phone:</dt>
                <dd>{{$contact->priceOffer->contact_phone}}</dd>
                <dt>Price Offer:</dt>
                <dd>
                    <a href="{{URL::to('/admin/price_offers/'.$contact->price_offer_id.'/edit')}}">
                        {{$contact->priceOffer->doc_nr}}
                        <button class="btn btn-xs btn-primary">Edit</button>
                    </a>
                </dd>
                <dt>Complaint message:</dt>
                <dd><blockquote>{!!nl2br(e($contact->complaint_message))!!}</blockquote></dd>
            </dl>

        </div>
    </div>
@stop
