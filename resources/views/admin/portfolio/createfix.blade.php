<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */?>
@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {{{ trans("admin/photo.photo") }}} @parent @stop



{{-- Content --}}
@section('main')
   <h3>
        <a href="{{URL::to('admin/')}}">{{trans('admin/admin.admin_panel')}}</a> >
        <a href="{{URL::to('admin/portfolio')}}">{{trans('admin/portfolio.title')}}</a> >
        Create
    </h3>
@if (count($errors) > 0)
	<div class="alert alert-danger">
	<strong>Whoops!</strong> There were some problems with your input.<br><br>
	<ul>
	@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
           </ul>
</div>
@endif

<div class="row">
    <div class="col-lg-6" > 

{!! Form::open(['url'=>'/admin/portfolio/create','files' =>'true']) !!}
 {!!Form::label('title','Title') !!}
 {!!Form::text('title',null,['class'=>'form-control']) !!}
 
  {!!Form::label('description','Description') !!}
  {!!Form::textarea('description',null,['class'=>'form-control']) !!}
   
    
  {!!Form::label('map','Folder name') !!}
 {!!Form::text('map',null,['class'=>'form-control']) !!}
 {!!Form::label('url','Url') !!}
 {!!Form::text('url',null,['class'=>'form-control']) !!}

  {!!Form::label('upload','Upload') !!}<br/>
        {{--<img id="image_upload_preview" src="" width="200" alt="foto" />--}}
  {!!Form::file("logo") !!}
        <div class="clear"><br></div>
  {!! Form::submit("Upload foto",['class'=>'btn  btn-danger'])!!}

 {!! Form::close() !!}
        <div class="clear"><br></div>
   <a href="/admin/portfolio" class=" btn btn-primary">Back</a>
    </div>
 </div>

   {{--<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>--}}
   {{--<script type="text/javascript">--}}
       {{--function readURL(input) {--}}
           {{--if (input.files && input.files[0]) {--}}
               {{--var reader = new FileReader();--}}

               {{--reader.onload = function (e) {--}}
                   {{--$('#image_upload_preview').attr('src', e.target.result);--}}
               {{--}--}}

               {{--reader.readAsDataURL(input.files[0]);--}}
           {{--}--}}
       {{--}--}}

       {{--$('[type="file"]').change(function () {--}}
           {{--readURL(this);--}}
       {{--});--}}



   {{--</script>--}}
@stop

{{-- Scripts --}}
@section('scripts')
    @parent
 
@stop


