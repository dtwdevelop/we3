
@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {{ trans("admin/photo.photo") }} @parent @stop



{{-- Content --}}
@section('main')
    <h3>
        <a href="{{URL::to('admin/')}}">{{trans('admin/admin.admin_panel')}}</a> >
        <a href="{{URL::to('admin/portfolio')}}">{{trans('admin/portfolio.title')}}</a> >
 Photos
    </h3>
@if (count($errors) > 0)
	<div class="alert alert-danger">
	<strong>Whoops!</strong> There were some problems with your input.<br><br>
	<ul>
	@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
           </ul>
</div>
@endif

<div class="row">
    <div class="col-lg-6" > 
     @foreach($fotos as $foto)
     <div>
     <?= HTML::image('appfiles/portfolio/'.$folder->map.'/'.$foto->title,$foto->title, array('class' => 'thumb',"width"=>100))  ?>
      <a  class="btn btn-success" href="{{ url('admin/portfolio/fotodelete',$foto->id) }}">Delete </a>

         <hr>
     </div>
     @endforeach

   <a href="/admin/portfolio" class=" btn btn-primary">Back</a>
    </div>
 </div>
 
@stop

{{-- Scripts --}}
@section('scripts')
    @parent
 
@stop


