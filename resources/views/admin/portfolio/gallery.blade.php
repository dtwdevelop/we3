<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>sample</title>

<style type="text/css">
body {margin:50px 0px; padding:0px; background-color: #000000;color: #ffffff;}
#content {width:620px; margin:0px auto;}
#desc {margin:10px; float:left; font-family: Arial, sans-serif; font-size: 12px;}
</style>

<!-- include CSS always before including js -->

<link href="{{asset('/lite/skins/tn3/tn3.css') }}" rel="stylesheet" type="text/css">
<!-- include jQuery library -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<!-- include tn3 plugin -->


<script src="{{ asset('/lite/js/jquery.tn3lite.min.js') }}"></script>

<!--  initialize the TN3 when the DOM is ready -->
<script type="text/javascript">
	$(document).ready(function() {
		//Thumbnailer.config.shaderOpacity = 1;
		var tn1 = $('.mygallery').tn3({
skinDir:"skins",
imageClick:"fullscreen",
image:{
maxZoom:1.5,
crop:true,
clickEvent:"dblclick",
transitions:[{
type:"blinds"
},{
type:"grid"
},{
type:"grid",
duration:460,
easing:"easeInQuad",
gridX:1,
gridY:8,
// flat, diagonal, circle, random
sort:"random",
sortReverse:false,
diagonalStart:"bl",
// fade, scale
method:"scale",
partDuration:360,
partEasing:"easeOutSine",
partDirection:"left"
}]
}
		});
	});
</script>
</head>
<body>
    <div id="content">
    <div class="mygallery">
	<div class="tn3 album">
	    <h4>Fixed Dimensions</h4>
	    <div class="tn3 description">Images with fixed dimensions</div>
	    <div class="tn3 thumb">images/35x35/1.jpg</div>
	    <ol>
                @foreach($files as $file)
		<li>
                   
		    <h4>{{$file}}</h4>
		    <div class="tn3 description">Foto</div>
		    <a href="/{{$file}}">
			<img src="/{{$file}}" />
                        
		    </a>
		</li>
		@endforeach
		
		
 </ol>
	</div>
    </div>
   
</div>
<a class="btn btn-success" href="{{ url("/admin/portfolio")}}">back</a>
</body>
</html>



