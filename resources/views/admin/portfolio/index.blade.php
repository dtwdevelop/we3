@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {{{ trans("admin/photo.photo") }}} @parent @stop

{{-- Content --}}
@section('main')
@if(Session::has('photo'))
<p class="alert alert-success">{{Session::get('photo') }}</p>
@endif
    <div class="page-header">
       
            <h3> <a href="{{URL::to('admin/')}}">{{trans('admin/admin.admin_panel')}}</a> >
                {{ trans("admin/portfolio.title") }}</h3>
            <a  class="btn btn-success" href="{{ url('admin/portfolio/add') }}">Create Porfolio</a>

            <div class="container" style="margin-top: 10px">
          
              @foreach($logos as $logo)
              <div class="row">
                  <div class="col-lg-3">
                      <?= HTML::image('appfiles/portfolio/'.$logo->logo,$logo->title, array('class' => 'thumb',"width"=>100))  ?>
                    
                      
                      </div>
                   <div class="col-lg-6">Description:  {{$logo->description}}</div>
         <a  class="btn btn-warning" href="{{ url('admin/portfolio/edit',$logo->id) }}">Edit</a>
         <a  class="btn btn-success" href="{{ url('admin/portfolio/fotolists',$logo->id) }}">Photos </a>
                <form action="{{ url('admin/portfolio/destroy',$logo->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="_token" value="{{ csrf_token() }}"> <button class="btn btn-danger" type="submit">Delete</button></form>

              </div>
                <div class="clear"><br></div>
              <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample{{$logo->id}}" aria-expanded="false" aria-controls="collapseExample">
  Upload photos
</button>
<div class="collapse" id="collapseExample{{$logo->id}}">
  <div class="well">
    @include("admin.portfolio.uploadfoto",['id'=>$logo->id])
  </div>
</div>
              <hr>
       
              
              @endforeach
              <?= $logos->appends(['sort' => 'title'])->render(); ?>
            </div>
        
    </div>

    
@stop

{{-- Scripts --}}
@section('scripts')
    @parent
 
@stop


