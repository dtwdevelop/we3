<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */?>
@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {{{ trans("admin/photo.photo") }}} @parent @stop



{{-- Content --}}
@section('main')
    <h3>
        <a href="{{URL::to('admin/')}}">{{trans('admin/admin.admin_panel')}}</a> >
        <a href="{{URL::to('admin/portfolio')}}">{{trans('admin/portfolio.title')}}</a> >
        Update
    </h3>
@if (count($errors) > 0)
	<div class="alert alert-danger">
	<strong>Whoops!</strong> There were some problems with your input.<br><br>
	<ul>
	@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
           </ul>
</div>
@endif

<div class="row">
    <div class="col-lg-6" > 

{!! Form::model($model,['url'=>"/admin/portfolio/update/".$model->id,'files' =>'true']) !!}
 {!!Form::label('title','Title') !!}
 {!!Form::text('title',null,['class'=>'form-control']) !!}
 
  {!!Form::label('description','Description') !!}
  {!!Form::textarea('description',null,['class'=>'form-control']) !!}
   
    
 
 {!!Form::hidden('map',null,['class'=>'form-control','value'=>$model->map]) !!}
 {!!Form::label('url','Url') !!}
 {!!Form::text('url',null,['class'=>'form-control']) !!}
  {!!Form::label('upload','Upload logo') !!}
  {!!Form::file("logo") !!}
        <div class="clear"><br></div>
  {!! Form::submit("Update",['class'=>'btn  btn-danger'])!!}
 {!! Form::close() !!}
        <br/>
   <a href="/admin/portfolio" class=" btn btn-primary">Back</a>
    </div>
 </div>
 
@stop

{{-- Scripts --}}
@section('scripts')
    @parent
 
@stop



