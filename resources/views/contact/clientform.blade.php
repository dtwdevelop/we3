<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
@extends('app') 
@section('content')

 @if (count($errors) > 0)
	<div class="alert alert-danger">
	<strong>Whoops!</strong> please check error.<br><br>
	<ul>
	@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
           </ul>
</div>
       
@endif



        
<h3>Please fill all fields *</h3>
  {!! Form::open(['url'=>'/contact/sendform','class'=>'clform','data-toggle'=>"validator",'role'=>"form"]) !!}
   {!! Form::hidden('id', $id) !!}
    <div class="help-block with-errors"></div>
   <div class="row"> 
       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

 <h3>Company details</h3>
@include("contact.form.company")
   
    </div>
   
     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-bottom: 20%;">  
            <h3> Information about your audience</h3>
@include("contact.form.information")
     </div>  
   
    
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h3>Website design and style</h3>
       @include("contact.form.design") 
   </div>
       
       
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
       <h3>Website functionality</h3>
        @include("contact.form.function") 
         {!! Form::submit("Send",['class'=>' btnc  btn btn-default'])!!}
         {!! Form::close() !!}
         </div>
  
  
 
   </div>
@endsection

@section('styles')

<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('scripts')
  @parent
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script src="/js/validator/validator.min.js"> </script>
<script type="text/javascript">
   // $('.clform').validator();
       $('nav').hide();
        $('select').select2();
//        $(".btnc").click(function(e){
//          // e.preventDefault();
//           var form = $('.formfull').html();
//           var token = $('input[name=_token]' ).val()
//           form ="sample"
//           $.post("/contact/sendform",{datas:form,_token:token},function(data){
//                console.log(data);
//           });
//          
//        });
</script>
@endsection

