<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
@extends('app')
@section('content')


<div class="wrap">
                            
<h2 class="m_1">Contact US</h2>
                   
                        
  
     
                         
<div class="footer-grid footer-grid1">
   @if (count($errors) > 0)
	<div class="alert alert-danger">
	<strong>Whoops!</strong> please check error.<br><br>
	<ul>
	@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
           </ul>
</div>
       
@endif
<div class="row"><div class="col-md-6" >                              
  {!! Form::open(['url'=>'/contact/send',"class"=>"formc"]) !!}
 {!!Form::label('company','Company') !!}
 {!!Form::text('company',null,['class'=>'form-control']) !!}
 
  {!!Form::label('name','Your name:') !!}
 {!!Form::text('name',null,['class'=>'form-control']) !!}
 
 
 {!!Form::label('email','Your email:') !!}
 {!!Form::text('email',null,['class'=>'form-control']) !!}
 
 
 {!!Form::label('phone','Phone:') !!}
 {!!Form::text('phone',null,['class'=>'form-control']) !!}
 
  {!!Form::label('message','Message') !!}
  {!!Form::textarea('message',null,['class'=>'form-control']) !!}
   
    
  
 
 
  {!! Form::submit("Send",['class'=>' btnc  btn btn-default'])!!}
 {!! Form::close() !!}
    </div></div>				
</div>
<div class="footer-grid footer-grid2">
				    <ul>
	<li>
					  <ul>
	<li>
						<div class="foot-img"><img src="images/phone-icon.png"/></div>
						<div class="extra-wrap">
							<p>{{cfg::get('contacts_phone')}}</p>
						</div></li>
						<li>
						<div class="foot-img"><img src="images/email-icon.png"/></div>
						<div class="extra-wrap1">
							<p>{{cfg::get('contacts_email')}}</p>
						</div></li>
						<li>
						<div class="foot-img"><img src="images/adress-icon.png"/></div>
						<div class="extra-wrap1">
							<p>{{cfg::get('contacts_address')}}</p>
						</div></li>
					</ul>
					
					<div class="location-map" id="map"></div>
					
					<div class="location-map" id="map"></div>
				</div>
				
				<div class="clear"> </div>
			</div>




<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
 <script type="text/javascript">
     
     $(document).ready(function(){
  $('.btnc').click(function(e){   
         e.preventDefault();
           
          var input = $(".formc").serialize();
   

  $.post("contact/send",input,function(data){
              alert("Thank for submit!");  
              $(".formc").hide();  
          
    }).fail(function(data) {
    alert(data.responseText);
});
    
    
  }); 
});
   </script>
   
  <div id="timesheet" style="width: 400px"></div>         
        
   

   <script src="/timesheet/timesheet.js"></script>    
      <script>
      new Timesheet('timesheet', 2015, 2016, [
  ['2015', '07/2015', 'Begin Project', 'lorem'],
  ['2015', '08/2015', 'Coding', 'ipsum'],
  ['2015', '11/2015', 'Finish','dolor'],
 
  
]);
      </script>
@endsection

 @section("styles")
      <link rel="stylesheet" href="/timesheet/timesheet.css">
    
  @endsection
   

