<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
@extends('app')
@section('contact')
<div class="row">
    <div class="col-lg-3">
        <h1>Contact us</h1>
        @if (count($errors) > 0)
	<div class="alert alert-danger">
	<strong>Whoops!</strong> There were some problems with your input.<br><br>
	<ul>
	@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
           </ul>
</div>
@endif
<h3>Contact form</h3>
 {!! Form::open(['url'=>'/contact/send']) !!}
 {!!Form::label('company','Company') !!}
 {!!Form::text('company',null,['class'=>'form-control']) !!}
 
  {!!Form::label('name','Your name:') !!}
 {!!Form::text('name',null,['class'=>'form-control']) !!}
 
 
 {!!Form::label('email','Your email:') !!}
 {!!Form::text('email',null,['class'=>'form-control']) !!}
 
 
 {!!Form::label('phone','Phone:') !!}
 {!!Form::text('phone',null,['class'=>'form-control']) !!}
 
  {!!Form::label('message','Message') !!}
  {!!Form::textarea('message',null,['class'=>'form-control']) !!}
   
    
  
 
 
  {!! Form::submit("Send",['class'=>'btn  btn-danger'])!!}
 {!! Form::close() !!}
    </div>
    <div class="col-lg-4">
        <div>Phone</div>
         <div>Email</div>
          <div>Adress</div>
          <div class="well ">Map</div>
          <div>Social form</div>
    </div>
</div>

@endsection

