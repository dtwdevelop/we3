{!!Form::label('company','Company name *') !!}
 {!!Form::text('company',null,['class'=>'form-control input-sm','required','data-error'=>'']) !!}
  {!!Form::label('companyage','Company age *') !!}
 {!!Form::text('companyage',null,['class'=>'form-control input-sm','required','data-error'=>'']) !!}
 {!!Form::label('webpage','Current web page (if exists) *') !!}
 {!!Form::text('webpage',null,['class'=>'form-control input-sm','required','data-error'=>'']) !!}
 {!!Form::label('industry:','Industry *') !!}
 {!!Form::text('industry:',null,['class'=>'form-control input-sm','required','data-error'=>'']) !!}
 {!!Form::label('productoff','Products offered by the company *') !!}
 {!!Form::text('productoff',null,['class'=>'form-control input-sm','required','data-error'=>'']) !!}
 {!!Form::label('productprice','Product price range *') !!}
 {!!Form::text('productprice',null,['class'=>'form-control input-sm' ,'required','data-error'=>'']) !!}
 
 {!!Form::label('companymiss','Company mission *') !!}
 {!!Form::text('companymiss',null,['class'=>'form-control input-sm','required','data-error'=>'']) !!}
  {!!Form::label('disired','Desired function of the website *') !!}
 {!!Form::text('disired',null,['class'=>'form-control input-sm','required','data-error'=>'']) !!}  
 {!!Form::label('aboutcompany','Any additional information about your company:') !!}
 {!!Form::text('aboutcompany',null,['class'=>'form-control input-sm']) !!} 
 
 <h4> Competitor information</h4>
 {!!Form::label('name','Name:') !!}
 {!!Form::text('name',null,['class'=>'form-control input-sm']) !!} 
  {!!Form::label('website','Website:') !!}
 {!!Form::text('website',null,['class'=>'form-control input-sm']) !!} 

  {!!Form::label('howare','How are you different from them?') !!}
 {!!Form::text('howare',null,['class'=>'form-control input-sm']) !!} 