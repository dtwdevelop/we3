 {!!Form::label('planned','Planned website name *') !!}
 {!!Form::text('planned',null,['class'=>'form-control input-sm','required','data-error'=>'']) !!} 
  <p>The website should have following sections *</p>
    {!!Form::select('websitefollow',array(
    'The website should have following sections *'=>[
 '1'=>' About Us,',
 '2'=>' News,',
 '3'=>' Contacts,',
 '4'=>' Product portfolio,',
  '5'=>' Product portfolio,',
  '6'=>' Galery,',
 
 ]
 
 ),null,['multiple'=>'multiple','name'=>'websitefollow[]','required','data-error'=>''])
    !!}
    
{!!Form::label('websitefollother','Other:') !!}
 {!!Form::text('websitefollother',null,['class'=>'form-control input-sm']) !!} 
 <p>What functionality do you expect from your website? *</p>
    {!!Form::select('functionwebsite', ['default' => '']+array(
 'What functionality do you expect from your website? *'=>[
 '1'=>' Publishing news,',
 '2'=>' Publishing articles,',
 '3'=>' Newsletter,',
 '4'=>' Publication of press releases and documents,',
  '5'=>' Possibility to create a product portfolio,',
  '6'=>' Possibility to put the goods in the baske,',
   '7'=>' The ability to pay for goods (online store),',
    '8'=>' The ability to store the goods in a basket (basket with memory),',
     '9'=>' Imports from XLS, and CSV,',
     '10'=>' Calculation of shipping costs depending on the weight,  delivery type, etc,',
      '11'=>' Virtual gift cards, vouchers, discounts (entering the code at purchase),',
        '12'=>' Affiliate, partnership program,',
          '13'=>' The ability to search the site,',
            '14'=>' Vacancy publication,',
              '15'=>' Publication of files to download,',
              '16'=>' Questionares,',
              '17'=>' Review Book,',
              '18'=>' Ad board,',
              '19'=>' Ad placement,',
              '20'=>' Rating system',
              '21'=>' Registration with different access levels,',
               '22'=>' Publication of events and calendars',
                '23'=>' RSS feed,',
                 '24'=>' Responsive website design,',
                  '25'=>' Retina-compatible design ,',
                   '26'=>' Video player availability,',
    ]    
 ),null,['multiple'=>'multiple','name'=>'functionwebsite[]' ,'required','data-error'=>''])
    !!}
{!!Form::label('othermodule','Other modules:') !!}
 {!!Form::text('othermodule',null,['class'=>'form-control input-sm']) !!} 
 
 {!!Form::label('otherlan','Language versions: ') !!}
 {!!Form::text('otherlan',null,['class'=>'form-control input-sm']) !!} 
 
  {!!Form::label('planupdate','Planned update frequency:') !!}
 {!!Form::text('planupdate',null,['class'=>'form-control input-sm']) !!}
  <p>Future content management will be done by *</p>
   {!!Form::select('contentman', array(
 'Future content management will be done by *'=>[
   '1'=>' The client,',
 '2'=>' WE 3,',
 '3'=>' Together,',
 ]
),null,['multiple'=>'multiple','name'=>'contentman[]','required','data-error'=>''])
    !!}
    
 {!!Form::label('special','Special requests for the website functionality:') !!}
 {!!Form::text('special',null,['class'=>'form-control input-sm']) !!} 


