
 {!!Form::label('location','Geographical location *') !!}
 {!!Form::text('location',null,['class'=>'form-control input-sm','required','data-error'=>'']) !!}

  
<p>Target audience *</p>
 {!!Form::select('target', array(
 'Target audience'=>[
 '1'=>' Kids,',
 '2'=>' Men,',
 '3'=>' Woman,',
 '4'=>' In a relationship,',
 '5'=>' Single,',
 ]
    ),null,['multiple'=>true, 'name'=>'target[]','required','data-error'=>''])
!!}
<p>Age Group</p>
{!!Form::select('agegroup[]',array(
'Age Group'=>[
 '1'=>' 0-17,',
 '2'=>' 18-24,',
 '3'=>' 25-35,',
 '4'=>' 36-50,',
  '5'=>' 51-70,',
   '6'=>' 51-70,',
   '7'=>' 70+,'
   ]
    ),null,['multiple'=>true])
!!}
<p>Annual income</p>
{!!Form::select('annual',array(
'Annual income'=>[
 '1'=>' £0 - £15,000,',
 '2'=>' £15,001 - £25,000,',
 '3'=>' £25,001 - £35,000,',
 '4'=>' £35,001 - £55,000',
  '5'=>' £55,001 - £100,000,',
   '6'=>' £100,000+ ,',
   ]
  
    ),null,['multiple'=>'multiple','name'=>'annual[]'])
!!}
{!!Form::label('audience','Audience\'s interests *') !!}
 {!!Form::text('audience',null,['class'=>'form-control input-sm','required','data-error'=>'']) !!}
 
 
 
  <p>Audience type:</p>
 {!!Form::select('audiencetyped', array(
 'Audience type:'=>[
 '1'=>' Conservative,',
 '2'=>' Modern,',
 '3'=>' Inovative,',
 ]
 ),null,['multiple'=>'multiple','name'=>'audiencetyped[]'])
    !!}
    
      <p>What are you looking to achieve with the website:</p>
 {!!Form::select('looking', array(
 'What are you looking to achieve with the website'=>[
 '1'=>' Attract clients and partners,',
 '2'=>' Product advertisement,',
 '3'=>' Branding,',
 '4'=>' To acquaint customers with the products,',
 '5'=>' Collect information for marketing purpouses,',
 '6'=>' Sales,',
 '7'=>' Communication with clients,',
 '8'=>' To reduce labour costs,',
 ]
 ),null,['multiple'=>'multiple','name'=>'looking[]'])
    !!}
    
{!! Form::label('rezult','Results of past marketing research (if possible):') !!}
 {!!Form::text('rezult',null,['class'=>'form-control input-sm']) !!}
 
 {!! Form::label('income','Marketing problems (possibility of negative outcome):') !!}
 {!!Form::text('income',null,['class'=>'form-control input-sm']) !!}