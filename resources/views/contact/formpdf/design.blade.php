<p>Website Type</p>
 {!!Form::select('webtype',array(
 'Website Type'=>[
 '1'=>' Corporate ,',
 '2'=>' Informative ,',
 '3'=>' Product portfolio,',
 '4'=>' Promotional website, ',
 ]
 ),null,['multiple'=>'multiple','name'=>'webtype[]'])
    !!}
     <br/>
    <p>Is there a corporate style/design in place?</p>
 {!!Form::select('corparative', array(
 'Is there a corporate style/design in place?'=>[
 '1'=>' Does not exist,',
 '2'=>' Style is available, but you can deviate from it,',
 '3'=>' Strict style (will be sent),',
 ]
 
 ),null,['multiple'=>'multiple','name'=>'corparative[]'])
    !!}
     <p>Freedom of action in the visual part of the design</p>
 {!!Form::select('freedom', array(
 'Freedom of action in the visual part of the design'=>[
 '1'=>' restricted, the design should be strict,',
 '2'=>' bold visual solutions are partly welcomed,',
 '3'=>' complete freedom of action in terms of the design,',
 ]
 
 ),null,['multiple'=>'multiple','name'=>'freedom[]'])
    !!}
     <br/>
     <p>Design has to be</p>
  {!!Form::select('desinghas', array(
  'Design has to be'=>[
 '1'=>' Modern,',
 '2'=>' Traditional,',
 '3'=>' Serious,',
 '4'=>' Friendly,',
 '6'=>' Corporate,',
 '7'=>' Open plan,',
 '8'=>' Luxury,',
 '9'=>' Affordable,',
 '10'=>' Strong,',
 '11'=>' Soft,',
 '12'=>' Loud,',
 '13'=>' Classic,',
  '14'=>' Romantic,',
  '15'=>' Minimalistic,',
  '16'=>' Bright,',
   '17'=>' Fun,',
    '18'=>' Historical,',
    '19'=>' Elegant,',
    '20'=>' Energetic,'
 ]
 ),null,['multiple'=>'multiple','name'=>'desinghas[]'])
    !!}
     <p>During the development we should focus on</p>
  {!!Form::select('during',array(
  'During the development we should focus on'=>[
 '1'=>' Website design,',
 '2'=>' Availability and visibility of the text,',
 '3'=>' Functionality and services,',
 ]
 
 ),null,['multiple'=>'multiple','name'=>'during[]'])
    !!}
     <br/>
    
       <p>Color scheme for the website</p>
     {!!Form::select('color',array(
     'Color scheme for the website'=>[

 '1'=>' Bright and effective colors,',
 '2'=>' Calm tones,',
 ]
 
 ),null,['multiple'=>'multiple','name'=>'color[]'])
    !!} <br/>
    
      <p>When visiting your website the client should</p>
     {!!Form::select('visit',array(
     'When visiting your website the client should'=>[
 '1'=>' See,',
 '2'=>' Do,',
 '3'=>' Feel,',
 ]
 
 ),null,['multiple'=>'multiple','name'=>'visit[]'])
    !!} <br/>
    
 {!!Form::label('opinion','In your opinion, the best website designs online (please provide a link):') !!}
 {!!Form::text('opinion',null,['class'=>'form-control input-sm']) !!} 
  <br/>
 {!!Form::label('worst','In your opinion, the worst website designs online (please provide a link):') !!}
 {!!Form::text('worst',null,['class'=>'form-control input-sm']) !!} 
  <br/>
  {!!Form::label('definitely','What should definitely be in your design:') !!}
 {!!Form::text('definitely',null,['class'=>'form-control input-sm']) !!} 
  <br/>
   {!!Form::label('no','What should definitely not be in your design:') !!}
 {!!Form::text('no',null,['class'=>'form-control input-sm']) !!} 


