
@extends('app') 
@section('content')

 @if (count($errors) > 0)
	<div class="alert alert-danger">
	<strong>Whoops!</strong> please check error.<br><br>
	<ul>
	@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
           </ul>
</div>
       
@endif

<div class="row">
    <div class="col-md-6">
        <h3>Whoops! It looks like you have already completed the client form!</h3>
    </div>
</div>

        
  
 
   </div>
@endsection

