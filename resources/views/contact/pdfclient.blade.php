<html>
    
    <body>
<div class="row">
    <div class="col-md-2 ">
        <h3>Company details</h3>
  {!! Form::model($from ,['url'=>'/contact/sendform','class'=>'clform']) !!}
{!!Form::label('id','Client id:') !!}
 {!! Form::text('id') !!}
 <br/>
@include("contact.formpdf.company")

 
    </div>
    <div class="col-md-2">
@include("contact.formpdf.information")
    </div>
      <div class="col-md-2 col-md-offset-2">
      <h3>Website design and style</h3>
       @include("contact.formpdf.design") 
        </div>
        <div class="col-md-2 col-md-offset-1">
       <h3>Website functionality</h3>
        @include("contact.formpdf.function") 

 {!! Form::close() !!}
    </div> 
</div>
</body>
</html>






