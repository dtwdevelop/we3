<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Contact message</title>
</head>
<body>
<h3>WE3UK System announcement:</h3>
<br>
New message received.<br>
<br>
Company name: {{$contact->company}}<br>
Name: {{$contact->name}}<br>
{{--Phone: {{$contact->phone}}<br>--}}
E-mail: {{$contact->email}}<br>
Message:<br>
<?php echo nl2br(htmlspecialchars($contact->message)); ?><br>
<br>
Check now: <a href="{{URL::to('/admin/contact?id='.$contact->id.'&search=1')}}">
    {URL::to('/admin/contact?id='.$contact->id.'&search=1')}}
</a><br>
</body>
</html>
