<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Price Offer</title>
</head>
<body>
Dear {{$name}},<br>
<br>
We have processed the information from your form and created a personalised Price Offer for your order which you can find at this link:<br>
<a href="{{URL::to('/price_offer/'.$hash)}}">{{URL::to('/price_offer/'.$hash)}}</a><br>
<br>
Please confirm the order at the end of the page so we can start working on your new website.<br>
<br>
<br>
Kind Regards,<br>
WE3 Team<br>
info@we3uk.com<br>
</body>
</html>
