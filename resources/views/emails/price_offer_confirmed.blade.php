<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Price Offer Confirmed</title>
</head>
<body>
Dear {{$price_offer->contact_person_name}},<br>
<br>
Thank you for confirming <a href="{{URL::to('/price_offer/'.$price_offer->hash)}}">Price offer {{$price_offer->doc_nr}}.</a><br>
<br>
We received your confirmation and will start working on your website.<br>
<br>
If you have any questions, please don’t hesitate to contact us.<br>
<br>
<br>
Kind Regards,<br>
WE3 Team<br>
info@we3uk.com<br>
</body>
</html>
