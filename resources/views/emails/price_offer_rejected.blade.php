<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Price Offer Rejected</title>
</head>
<body>
Dear {{$price_offer->contact_person_name}},<br>
<br>
Thank you for sending us your message.<br>
<br>
We will check it and fix your <a href="{{URL::to('/price_offer/'.$price_offer->hash)}}">Price offer {{$price_offer->doc_nr}}</a> soon and contact you.<br>
<br>
<br>
Kind Regards,<br>
WE3 Team<br>
info@we3uk.com<br>
</body>
</html>
