<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Price Offer Rejected</title>
</head>
<body>
<h3>WE3UK System announcement:</h3>
<br>
Client has <b>rejected</b> <a href="{{URL::to('/price_offer/'.$price_offer->hash)}}">Price offer {{$price_offer->doc_nr}}.</a><br>
<br>
Company name: {{$price_offer->company_name}}<br>
Contact person name: {{$price_offer->contact_person_name}}<br>
Contact email: {{$price_offer->contact_email}}<br>
Contact phone: {{$price_offer->contact_phone}}<br>
<br>
View message: <a href="{{URL::to('/admin/messages/'.$price_offer->id)}}">{{URL::to('/admin/messages/'.$price_offer->id)}}</a>.<br>
</body>
</html>
