<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Quote Form</title>
</head>
<body>
Dear {{$name}},<br>
<br>
For us to understand how you want your website to look and function, we would require some information.<br>
<br>
Please fill in a Quote Form at the following link - <a href="{{URL::to('/quote_form/'.$hash)}}">{{URL::to('/quote_form/'.$hash)}}</a>.<br>
<br>
Once we receive your form, we will price your order and reply to you with a Price Offer.<br>
<br>
If you have any questions, please don’t hesitate to contact us.<br>
<br>
<br>
Kind Regards,<br>
WE3 Team<br>
info@we3uk.com<br>
</body>
</html>
