<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Quote Form</title>
</head>
<body>
Dear {{$name}},<br>
<br>
Thank you for filling Quote Form.<br>
<br>
We received your form and will send you a Price Offer soon.<br>
<br>
You can view your Quote Form in PDF format at this link: <a href="{{$quote_form->getPdfUrl()}}">{{$quote_form->getPdfUrl()}}</a>.<br>
<br>
If you have any questions, please don’t hesitate to contact us.<br>
<br>
<br>
Kind Regards,<br>
WE3 Team<br>
info@we3uk.com<br>
</body>
</html>
