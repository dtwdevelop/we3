<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Quote Form</title>
</head>
<body>
<h3>WE3UK System announcement:</h3>
<br>
Client has filled Quote Form.<br>
<br>
Contact ID: {{$quote_form->contact->id}}<br>
Company name: {{$quote_form->contact->company}}<br>
Contact person name: {{$quote_form->contact->name}}<br>
Contact email: {{$quote_form->contact->email}}<br>
Contact phone: {{$quote_form->contact->phone}}<br>
<br>
Download PDF: <a href="{{$quote_form->getPdfUrl()}}">{{URL::to('/quote_form/'.$quote_form->getPdfUrl())}}</a>.<br>
</body>
</html>
