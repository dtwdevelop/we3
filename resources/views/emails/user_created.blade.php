<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>WE3 account created</title>
</head>
<body>
Dear {{$user->name}},<br>
<br>
Your admin account for WE3 Admin panel has been created.<br>
<br>
Login URL: <a href="{{URL::to('/admin')}}">{{URL::to('/admin')}}</a><br>
Login email: <b>{{$user->email}}</b><br>
Login password: <b>{{$user->password}}</b><br>
<br>
<br>
Kind Regards,<br>
WE3 Team<br>
info@we3uk.com<br>
</body>
</html>
