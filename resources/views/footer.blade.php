<footer id="footer">
    <div class="skew-container">
        <div class="skew-line">
            <div class="skew-top-right">
                <a class="scroll-to" href="#head"><div class="scroll-top-button">TOP</div></a>
            </div>
        </div>
    </div>
    <div class="container onscroll-animate">
        <p><i class="fa fa-map-marker fa-2x"></i></p>
        <p>20 Longhill Road<br/>
            London SE6 1TY <br/>  United Kingdom </p>
        <div class="margin-40"></div>
        <p><i class="fa fa-mobile fa-2x"></i></p>
        <p>Tel: +44 (0) 77 1973 4147</p>
        <div class="margin-40"></div>
        <p><i class="fa fa-envelope-o fa-2x"></i></p>
        <p>Email: <a href="mailto:info@wethree.eu">info@wethree.eu</a></p>
    </div>
    <div class="copyright">
        <a href="http://www.facebook.com/we3uk" target="_blank"><i class="social-icon fa fa-facebook"></i></a>
        <a href="https://twitter.com/wethreeuk" target="_blank"><i class="social-icon fa fa-twitter"></i></a>
        <a href="http://linkd.in/1Lwm6DP" target="_blank"><i class="social-icon fa fa-linkedin"></i></a>
    </div>
</footer>