<!DOCTYPE html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" xml:lang="en-gb" class="no-js"> <!--<![endif]--><head>
    <?php
    use App\DBConfiguration as cfg;
    ?>
    <meta charset="utf-8">
    <!--[if lt IE 9]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" media="handheld">
    <meta name='viewport' content='width=device-width'>
    <![endif]-->
    <meta name="description" content="{{cfg::get('site_description')}}">
    <meta name="keywords" content="{{cfg::get('site_keywords')}}">
    <meta name="author" content="Price offer">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="icon.html" type="image/png">
        <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
        <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Icon-Font -->
    <link rel="stylesheet" href="/font-awesome/font-awesome/css/font-awesome.min.css" type="text/css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/font-awesome/font-awesome/css/font-awesome-ie7.min.css" type="text/css">
    <![endif]-->
        <link rel="stylesheet" type="text/css" href="http://cdn.knightlab.com/libs/timeline/latest/css/timeline.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/owl-carousel/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="/lightbox/css/lightbox.css" type="text/css">
    <link rel="stylesheet" href="/masterslider/style/masterslider.css" type="text/css">
    <link rel="stylesheet" href="/masterslider/skins/light-6/style.css" type="text/css">
    <link rel="stylesheet" href="/styles/main.css" type="text/css">
    <link rel="stylesheet" href="/styles/custom.css" type="text/css">
        <script type="text/javascript" src="/js/modernizr.js"></script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>

   {{--<script type="text/javascript" src="http://cdn.knightlab.com/libs/timeline/latest/js/timeline.js"></script>--}}
    <!-- or -->
        <script type="text/javascript">
        //    window.onresize = function(event)
        //{
        //  document.location.reload(true);
        //  } *//
    </script>
        <title>{{cfg::get('site_title')}}</title>
</head>