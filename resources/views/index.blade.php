@include('header')


<body>
	<div id="all">
	<a href="/"><div id="logo">
	</div></a>
        <nav class="navbar navbar-default">
    <ul class="nav navbar-nav">
                <li><a href="#team">About Us</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#device-section">Portfolio</a></li>
                <li><a href="#contact">Contact Us</a></li>
    </ul>
    </nav>
        <div id="content">
            <div id="head">
        			   <div id="slider-shadow">
        			   
        			               <div class="ms-layer">
        			                       <div class="ms-layer-content">
        			                           
        			                           <h2>Design. Development. Dedication.</h2>
                                           </div><!-- .master-slider -->
        			   </div><!-- .ms-fullscreen-template --></div>    
                <div class="head-container">
                </div><!-- .head-container -->
            </div><!-- #head -->
        </div>
        <div id="system-modal" class="modal fade"  role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h2></h2>
                    </div>
                    <div class="modal-body">
                        <p> Thank you for contacting us, your request will be processed shortly</p>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
            <section id="team">
                <div class="section-content">
                    
                    <div class="container">
                        
                        <header class="header">
                            <h1>About Us</h1>
                            <p id="angletext" class="angletext onscroll-animate"> {{App\DBConfiguration::get('site_about_us')}}</p>
                        </header>
                    </div><!-- .container -->
                </div><!-- .section-content -->
            </section>
        <section id="services">
                <div class="section-content section-image-light parallax-background">
                   
                    <div class="container onscroll-animate">
                        <header class="header right">
                            <h1>Services</h1>
                           
                        </header>
{{--loop--}}  @if (!$services->isEmpty())
                            @foreach ($services as $service)
                        <div class="col-md-4">
                            <div class="blog-post">
                                <div class="post-header">
                                    <h3>{{$service->title_en}}</h3>

                                </div>
                                <div class="post-image">
                                    <img alt="blog post image" src="{{$service->getIconUrl()}}" alt=""/>


                                </div>

                                <div class="post-excerpt">
                                    @foreach($service->getDescriptions() as $description)
                                    <p>{{$description->title_en}}</p>
                                    @endforeach
                                </div>
                            </div><!-- .blog-post -->
                        </div><!-- .col-md-12 -->
                            @endforeach
                        @endif
  {{--endloop                      --}}
                    </div><!-- .section-content -->
                    </div>
            </section>
            
            <section id="device-section">
                <div class="section-content section-color">
                    <div class="container">
                        <header class="header center">
                            <h1 class="huge-heading onscroll-animate">
                               PORTFOLIO
                         	</h1>
                        </header>
                         
                        <img class="img-responsive onscroll-animate" alt="devices" src="images/portfolio-mma.png" style="position: relative !important;top: 52px;">
                    	<div class="margin-50"></div>
                    </div>
                    <div class="skew-line bottom-top">
                        <div id="skewindex" class="skew-bottom-right"></div>
                    </div>
                </div><!-- .section-content -->
            </section>
            
            <section id="portfolio">
                <div class="section-content">
                    <div class="container works">
                    <p id="angletext2" class="angletext onscroll-animate">Whilst working with Capsilite we had a chance to use all of our knowledge and skills in the field of web development . Such as the creation of corporate identity and logo design, web design and development of a bespoke CMS.</p>
                       <img class="img-responsive onscroll-animate" id="backwards" alt="devices" src="images/portfolio-reversescreen.png" style="position: relative !important;">

                    </div><!-- .container -->
                </div><!-- .section-content -->
            </section>
        <section id="subscribe">
            </section>
        <section id="map">
                <div class="section-content">
                    
                    
                    <!-- Google map -->
                    <div class="google-map onscroll-animate">
                        <div id="map-canvas"></div>
                    </div>
                    <!-- /Google map -->
                    
                    <div class="skew-line bottom-top">
                      
                    </div>
                </div><!-- .section-content -->
            </section>
        <section id="contact">
                <div class="section-content">
                    <div class="container">
                        <header class="header center onscroll-animate">
                            <h1>Contact form</h1>
                            <p>Say hello to us, we will answer as soon as possible.</p>
                        </header>
                        <div class="margin-20"></div>
                        <form data-toggle="validator" role="form" class="form-contact onscroll-animate" id="contact-form" action="/contact/send" method="post" data-all-fields-required-msg="All fields are required" data-ajax-fail-msg="Contact not send" data-success-msg="">
                            <div class="row">
                                <div class="col-md-4">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                    <p><input required  type="text" name="name" data-error="Name required" placeholder="Your name"></p>
                                    <span class="help-block with-errors"></span>
                                    </div>
                                    <div class="form-group">
                                    <p><input required  type="text" data-error="Phone required" name="phone" placeholder="Phone number"></p>
                                        <span class="help-block with-errors"></span>
                                        </div>
                                    <div class="form-group">
                                    <p><input required  type="email" data-error="Email required" name="email" placeholder="Email"></p>
                                        <span class="help-block with-errors"></span>
                                        </div>
                                    <div class="form-group">
                                    <p><input required  type="text" name="subject" data-error="Company required" placeholder="Company"></p>
                                        <span class="help-block with-errors"></span>
                                        </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                    <textarea required name="message" data-error="Message required"  placeholder="Your Message"></textarea>
                                        <span class="help-block with-errors"></span>
                                        </div>
                                </div>
                            </div>
                            <p class="text-right"><input type="submit" value="Send mail"></p>
                            <p class="return-msg"></p>
                        </form>
                    </div><!-- .container -->
                </div><!-- .section-content -->
            </section>
        @include('footer')
        </div><!-- #content -->
    </div><!-- #all -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="owl-carousel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="lightbox/js/lightbox.min.js"></script>
	<script type="text/javascript" src="masterslider/masterslider.min.js"></script>
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="js/jquery.inview.min.js"></script>
    <script type="text/javascript" src="js/jquery.countTo.js"></script>
    <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/placeholder-fallback.js"></script>
    <script type="text/javascript" src="/js/validator/validator.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>

</body>
</html>
