<?php
use App\DBConfiguration as cfg;
?>

@extends('_frontend')

@section('body')
<body>

<div class="slider-shadow">
	<div class="wrap"> 
	<div class="header" id="dynamic">
		
		<nav class="row clearfix">
			<div class="logo">
							<a href="#" class="totop clearfix"><img class="logo-image" src="images/logo.png" alt="We3 Homepage"></a>
	    		    	</div>
			
			<div id="nav" class="clearfix">
				<div class="special clearfix">
											<div class="menu-main-container"><ul id="menu-main" class="nav">
  <li><a class="cls" href="#services">SERVICES</a></li>
                    <li><a  class="cls" href="#about-us">ABOUT US</a>
                    <li><a  class="cls" href="#contacts">CONTACTS</a></li> 
</ul></div>								 	</div>
			</div>
		</nav>	
	</div>
	<!--fixed menu-->
		<div class="header-top" id="menu">
		  <div id="menucc">
		  <div class="logo">
				<a href="/"><img src="images/logo.png" alt=""/></a>
			 </div>
		     <div class="h_menu4"><!-- start h_menu4 -->
				<a class="toggleMenu" href="#">Menu</a>
				<ul class="nav">
                                    <li><a class="cls" href="#services">SERVICES</a></li>
					<li><a  class="cls" href="#about-us">ABOUT US</a>
					<li><a  class="cls" href="#contacts">CONTACTS</a></li>
				</ul>
				<script type="text/javascript" src="js/nav.js"></script>
			</div><!-- end h_menu4 -->
			<div class="clear"></div>
		  </div>
		</div><!-- end header_main4 -->

        @if(Session::has('message'))
            <?php $type = (Session::has('message-type')) ? Session::get('message-type') : 'success'; ?>
            <div class="alert alert-{{$type}}" role="alert">
                {{ Session::get('message') }}
            </div>
        @endif

        <div class="slider">
				<!---start-da-slider----->
			  <div id="da-slider" class="da-slider">
				  <div class="da-slide">
					<h1>{!! cfg::get('site_name') !!}</h1>


					<div class="button-intouch"><a href="#contacts" class="da-link cls">GET IN TOUCH</a></div>

				  </div>		
				  <!--<nav class="da-arrows">
					<span class="da-arrows-prev"></span>
				 	<span class="da-arrows-next"></span>
				  </nav>-->
			   </div>
 		       <!---//End-da-slider----->
	      </div>
	 </div>
<div>
	 <div class="main">
	     <div class="wrap">
	 	   <h2 class="m_1" id="services">Services</h2>

             <div class="content-top">
             @if (!$services->isEmpty())
                 @foreach ($services as $service)
	 	    	<div class="col_1_of_4 span_1_of_4">
					
					<div class="desc">
						<h3>{{$service->title_en}}</h3>
						<img src="{{$service->getIconUrl()}}" alt=""/>
						<ul>
                            @foreach($service->getDescriptions() as $description)
                                <li>{{$description->title_en}}</li>
                            @endforeach
						</ul>
					</div>
				</div>
                 @endforeach
                 @endif
				<div class="clear"></div>
		     </div>
		   
			</div>
	    </div>
	    
	      <div class="m_3" id="about-us"><span class="left_line"></span> About Us<span class="right_line"> </span>
		     <div class="content-middle-bottom">
	 	    	<div class="col_1_of_middle span_1_of_middle">
                    <p>{{cfg::get('site_about_us')}}</p>
	 	    	</div>
	 	   </div>
			   </div>
<!--FOOTER-->
	    <div class="footer" id="contacts">
			<div class="wrap">
			<h2 class="m_1">Contact US</h2>
				<div class="footer-grid footer-grid1">
  @if (count($errors) > 0)
	<div class="alert alert-danger">
	<strong>Whoops!</strong> please check error.<br><br>
	<ul>
	@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
           </ul>
</div>
       

@endif

                              
 <div class="modal fade bs-example-modal-sm popmod" id="popmod" tabindex="-1" style="z-index: 99999" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm pop mod">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
        Thank you for contacting us, your request will be processed shortly
      </div>
    
    </div>
  </div>
</div>



      {!! Form::open(['url'=>'/contact/send',"class"=>"formc"]) !!}
      
      {!! Form::text('company',null,['class'=>'form-control', 'placeholder' => 'enter your company name']) !!}
      {!! Form::text('name',null,['class'=>'form-control','placeholder' => 'enter your name']) !!}
      {!! Form::text('email',null,['class'=>'form-control','placeholder' => 'your email adress']) !!}
      {!! Form::text('phone',null,['class'=>'form-control','placeholder' => 'enter your phone number']) !!}
      {!! Form::textarea('message',null,['id' => 'message-textarea', 'class'=>'form-control','placeholder' => 'enter your message']) !!}
      {!! Form::submit("Send",['class'=>' btnc  btn btn-default']) !!}
      {!! Form::close() !!}

				</div>
				<div class="footer-grid footer-grid2">
				   <ul>
	<li>
						<div class="foot-img"><img src="images/phone-icon.png"/></div>
						<div class="extra-wrap">
							<p>{{cfg::get('contacts_phone')}}</p>
						</div></li>
						<li>
						<div class="foot-img"><img src="images/email-icon.png"/></div>
						<div class="extra-wrap1">
							<p><a href="mailto:info@wethreeuk.com">{{cfg::get('contacts_email')}}</a></p>
						</div></li>
						<li>
						<div class="foot-img"><img src="images/adress-icon.png"/></div>
						<div class="extra-wrap1">
							<p>{{cfg::get('contacts_address')}}</p>
						</div></li>
					</ul>
					<div class="social">	
			    <p>Follow us on social media</p>
				   <ul>	
					  <li class="facebook"><a href="https://www.facebook.com/we3uk" target="_blank"><span> </span></a></li>
					  <li class="twitter"><a href="https://twitter.com/wethreeuk" target="_blank"><span> </span></a></li>	
					  <li class="linkedin"><a href="#"><span> </span></a></li>
				   </ul>
			    </div>
					
				</div>
				
				<div class="clear"> </div>
			</div>
		</div>
		<div class="footer-bottom">
	 		  <div class="wrap">
	     	  	<div class="copy">
				   <p>© 2015 {{cfg::get('site_title')}}
				   <br />
				   <a href="/" target="_blank">Terms & Conditions</a></p>
			    </div>
			    
			    <div class="clear"></div>
			  </div>
       </div>
</div>

<!--<script type="text/javascript" src="js/jquery.min.js"></script>-->

    
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/modernizr.custom.28468.js"></script>

<script type="text/javascript" src="js/jquery.cslider.js"></script>
<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/animatescroll.min.js"></script>
<script type="text/javascript" src="/js/site/content.js"></script>

  <script type="text/javascript">
/*global jQuery:false */
jQuery(document).ready(function ($) {
    "use strict";

/*-----------------------------------------------------------------------------------*/
/*  1. FIXED SCROLL MENU
/*-----------------------------------------------------------------------------------*/

    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
        if (scroll >= 120 && $(window).width() >= 960) {
            $("#dynamic").addClass("show"), {duration:1000};
        } else {
            $("#dynamic").removeClass("show");
        }
    });
 
/*-----------------------------------------------------------------------------------*/
/*  2. NAVIGATION WAYPOINTS
/*-----------------------------------------------------------------------------------*/
       


/*-----------------------------------------------------------------------------------*/
/*  3. SCROLLING NAVIGATION
/*-----------------------------------------------------------------------------------*/

//    $('.nav a').click(function() {
//        $.scrollTo($(this).attr('href'), {duration: 2000, offset:-250});
//        return false;
//    });
//
//    $('.mobile-menu-inner .nav-mobile li a').click(function() {
//        $.scrollTo($(this).attr('href'), {duration: 2000, offset:0});
//        return false;
//    });
//
//});
/*-----------------------------------------------------------------------------------*/
/*  4. MOBILE NAVIGATION
/*-----------------------------------------------------------------------------------*/
    $("#collapse").hide();
    $("#collapse-menu").toggle(function () {
        
        $("#collapse").slideToggle(300);
        return false;

        }, function () {

        $("#collapse").slideToggle(300);
        return false;
    });
});
</script>
</body>

@endsection
