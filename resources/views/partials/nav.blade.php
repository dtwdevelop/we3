<nav class="navbar navbar-default navbar-inverse navbar-fixed-top">
    <div class="container" style="width: 95%;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{URL::to('admin/')}}"><img src="/favicon.png" style="width: 18px; display: inline-block; margin-right: 8px;" /> {{trans('admin/admin.admin_panel')}}</a>
        </div>

        @if (Auth::check())
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ (Request::is('admin') ? 'active' : '') }}">
                    <a href="{!! URL::to('admin/') !!}"><i class="fa fa-home"></i> {{trans('admin/admin.home')}}</a>
                </li>
                <li class="{{ (Request::is('admin/contact') ? 'active' : '') }}">
                    <a href="{!! URL::to('admin/contact') !!}"><i class="fa fa-comment"></i> Messages</a>
                </li>
                <li class="{{ (Request::is('admin/services') ? 'active' : '') }}">
                    <a href="{!! URL::to('admin/services/') !!}"><i class="glyphicon glyphicon-list"></i> {{trans('admin/services.services')}}</a>
                </li>
                <li class="{{ (Request::is('admin/portfolio') ? 'active' : '') }}">
                    <a href="{!! URL::to('admin/portfolio') !!}">
                        <i class="fa fa-folder-open"></i>
                        Portfolios</a>
                </li>
                <li class="{{ (Request::is('admin/sliders') ? 'active' : '') }}">
                    <a href="{!! URL::to('admin/sliders') !!}"><i class="fa fa-th-large"></i> {{trans('admin/sliders.sliders')}}</a>
                </li>
                <li class="{{ (Request::is('admin/users') ? 'active' : '') }}">
                    <a href="{!! URL::to('admin/users') !!}"><i class="glyphicon glyphicon-user"></i> {{trans('admin/admin.users')}}</a>
                </li>
                <li class="{{ (Request::is('admin/config') ? 'active' : '') }}">
                    <a href="{!! URL::to('admin/config') !!}"> <i class="fa fa-cog"></i>
                         Configurations</a>
                </li>
                <li class="{{ (Request::is('admin/price_offers') ? 'active' : '') }}">
                    <a href="{!! URL::to('admin/price_offers') !!}"> <i class="fa fa-tasks"></i>
                        Price offers</a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li class="{{ (Request::is('auth/login') ? 'active' : '') }}"><a href="{!! URL::to('auth/login') !!}"><i
                                    class="fa fa-sign-in"></i> Login</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->name }} <i
                                    class="fa fa-caret-down"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            @if(Auth::check())
                                @if(Auth::user()->admin==1)
                                    <li>
                                        <a href="{!! URL::to('admin/dashboard') !!}"><i class="fa fa-tachometer"></i> Dashboard</a>
                                    </li>
                                @endif
                                <li role="presentation" class="divider"></li>
                            @endif
                            <li>
                                <a href="{!! URL::to('auth/logout') !!}"><i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
        @endif
    </div>
</nav>