@include('header')
<body class="offer">
<div id="all">
    <a href="/"> <div id="logo">
    </div></a>
    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li><a href="#price">Price</a></li>
            <li><a href="#timeline">Timeline</a></li>
            <li><a href="#device-section">Portfolio</a></li>
            <li><a href="#contact">Confirmation</a></li>
        </ul>
    </nav>
    <nav id="mobilenav" role="navigation">
        <div class="block">
            <h2 class="block-title">Chapters</h2>
            <ul>
                <li>
                    <a href="#">Chapter 1</a>
                </li><!--
                   --><li>
                    <a href="#">Chapter 2</a>
                </li><!--
                   --><li>
                    <a href="#">Chapter 3</a>
                </li><!--
                   --><li>
                    <a href="#">Chapter 4</a>
                </li><!--
                   --><li>
                    <a href="#">Chapter 5</a>
                </li>
            </ul>
            <a class="close-btn" id="nav-close-btn" href="#top">Return to Content</a>
        </div>
    </nav>
    <div id="content2" style="min-height:60% !important;">
        <div id="head" style="height: 60% !important; min-height:60% !important;">
            <div id="slider-shadow">
                <div class="ms-layer">
                    <div class="ms-layer-content">
                        <br>
                        <h2>Price offer Nr:  {{ $price_offer->doc_nr }}</h2>
                        <br>
                        <div  class="client">
                            <p class="name"><span class="client_info_label">Company name:</span><span class="client_info_data"> {{ $price_offer->company_name}}</span></p>
                            <p class="company"><span class="client_info_label">Contact person name:</span><span class="client_info_data"> {{ $price_offer->contact_person_name }}</span></p>
                            <p class="email"><span class="client_info_label">Contact e-mail:</span><span class="client_info_data"> {{ $price_offer->contact_email}}</span></p>
                            <p class="phone"><span class="client_info_label">Contact phone:</span><span class="client_info_data"> {{ $price_offer->contact_phone}}</span></p>
                        </div>
                    </div><!-- .master-slider -->
                </div><!-- .ms-fullscreen-template --></div>
            <div class="head-container">
            </div><!-- .head-container -->
        </div><!-- #head -->
    </div>

</div>
<div id="system-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2></h2>
            </div>
            <div class="modal-body">
                <p class="modmsg"> Thank you for sending us a message. We will check it soon and fix your Price Offer. </p>
            </div>
            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>
<section id="price">
        <div class="section-content section-image-light parallax-background">
            <div class="container onscroll-animate">
                <header class="header right">
                    <h1>PRICE</h1>
                </header>
                <?php $total_sum = 0; ?>
                @foreach($servicesContainer as $service_id => $serviceContainer)
                    <?php $sum = 0; ?>
                <div class="col-md-4">
                    <div class="blog-post">
                        <div class="post-header">
                            <h3>{{$servicesDataContainer[$service_id][0]}}</h3>

                        </div>
                        <div class="post-image">
                            <img src="{{$servicesDataContainer[$service_id][1]}}" alt=""/>
                        </div>
                        <div class="post-excerpt">
                            @foreach($serviceContainer as $service)
                                <p>{{$service->label}}</p>
                                <?php $sum += $service->price; ?>

                                <p class="price">&pound;{{$service->price}} </p>

                            @endforeach
                                <p class="subtotal">Subtotal:  &pound;{{ number_format($sum,2) }} </p>
                        </div>
                    </div><!-- .blog-post -->
                </div><!-- .col-md-12 -->
                        <?php $total_sum += $sum; ?>
                    @endforeach<!-- .col-md-12 -->
                    <div class="col-md-12">
                <p class="bigsumm"> TOTAL:  &pound;{{$total_sum}}
                </p>
                        </div>
            </div><!-- .section-content -->
            </div>
    </section>
@if(Session::has('message'))
    <?php $type = (Session::has('message-type')) ? Session::get('message-type') : 'success'; ?>
    <div class="alert alert-{{$type}}" role="alert">
        {{ Session::get('message') }}
    </div>
    @endif
<!--timeline-->
    <section id="timeline">
        <div class="section-content">

            <div class="container">
                <div id="margin-40"></div>
                <header class="header">     <h1>Timeline</h1>       </header>
                <div  id="mytimeline">
                </div>
                <div id="timeline-embed"></div>
                <script type="text/javascript" src="http://cdn.knightlab.com/libs/timeline/latest/js/storyjs-embed.js"></script>
                </header>
            </div><!-- .container -->
        </div><!-- .section-content -->
    </section>
<!--portfolio-->
    <section id="device-section">
        <div class="section-content section-color">
            <div class="container">
                <header class="header center">
                    <h1 class="huge-heading onscroll-animate">
                        PORTFOLIO
                    </h1>
                </header>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            @foreach($portfolios as $portfolio)
                            <div class="col-md-6 col-sm-6 profile onscroll-animate fadeIn" >
                                <div class="profile-photo">
                                    <a href="{{ url('appfiles/portfolio/' . $portfolio->logo) }}" data-lightbox="image-1" data-title="My caption">
                                        <?= HTML::image('appfiles/portfolio/' . $portfolio->logo, $portfolio->title, array( 'class'=>"img-thumbnail")) ?>
                                    </a>
                                    <div class="image-detail-wrapper">
                                        <div class="image-detail-content"><a href="/index.php/timelinedetail"></a></div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div></div>
                <div class="margin-50"></div>
            </div>
        </div><!-- .section-content -->
    </section>
    <section id="portfolio">
        <div class="section-content">

        </div><!-- .section-content -->
    </section>
    <section id="subscribe">
    </section>
    <section id="contact">
        <div class="section-content">
            <div class="container conf">
                <header class="header center onscroll-animate">
                    @if(isset($price_offer->contact))
                    @if ($price_offer->contact->price_offer_status === 2)
                        <h2 class="m_1" style="color: #5A5">ORDER CONFIRMED</h2>
                    @else
                        <h2 class="m_1">ORDER CONFIRMATION</h2>
                    @endif
                    <p class="text-center">
                        @if ($price_offer->contact->price_offer_status !== 2)
                        <form action="{{Request::url().'/confirm'}}" id="contact-confirm" method="post" >
                        <input  name="but" class="but" id="but" type="submit" value="CONFIRM YOUR ORDER">
                    </form>
                            </p><br><br>
                    <p>If we have missed anything please <a id="confirm-complaint">send us a message</a></p>
                </header>
                <form class="form-contact onscroll-animate fadeIn" id="contact-form2" action="{{Request::url().'/send-message'}}" method="post" data-all-fields-required-msg="All fields are required" data-ajax-fail-msg="Ajax could not set the request" data-success-msg="">
                    <div class="middlinput">
                        <textarea name="message"  placeholder="Your Message"></textarea>
                        <p class="text-right"><input type="submit" value="Send mail"></p>
                        <p class="return-msg"></p>
                    </div>
                </form>
                @endif
                @else
                    @if ($price_offer->price_offer_status === 2)
                        <h2 class="m_1" style="color: #5A5">ORDER CONFIRMED</h2>
                    @else
                        <h2 class="m_1">ORDER CONFIRMATION</h2>
                    @endif
                    <p class="text-center">
                    @if ($price_offer->price_offer_status !== 2)
                        <form action="{{Request::url().'/confirm2'}}" id="contact-confirm" method="post" >
                            <input  name="but" class="but" id="but" type="submit" value="CONFIRM YOUR ORDER">
                        </form>
                        </p><br><br>
                        <p>If we have missed anything please <a id="confirm-complaint">send us a message</a></p>
                        </header>
                        <form class="form-contact onscroll-animate fadeIn" id="contact-form2" action="{{Request::url().'/send-messages'}}" method="post" data-all-fields-required-msg="All fields are required" data-ajax-fail-msg="Ajax could not set the request" data-success-msg="">
                            <div class="middlinput">
                                <input type="hidden" name="price_message" value="{{$price_offer->id}}">
                                <textarea name="message"  placeholder="Your Message"></textarea>
                                <p class="text-right"><input type="submit" value="Send mail"></p>
                                <p class="return-msg"></p>
                            </div>
                        </form>
                    @endif

                @endif
                <div class="margin-20"></div>
            </div><!-- .container -->
        </div><!-- .section-content -->
    </section>
    @include('footer')
    <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="http://cdn.knightlab.com/libs/timeline/latest/js/timeline-min.js"></script>
{{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>--}}
<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="/lightbox/js/lightbox.min.js"></script>
<script type="text/javascript" src="/masterslider/masterslider.min.js"></script>
<script type="text/javascript" src="/js/jquery.stellar.min.js"></script>
<script type="text/javascript" src="/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="/js/jquery.inview.min.js"></script>
<script type="text/javascript" src="/js/jquery.countTo.js"></script>
<script type="text/javascript" src="/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="/js/placeholder-fallback.js"></script>
    <script type="text/javascript" src="http://cdn.knightlab.com/libs/timeline/latest/js/storyjs-embed.js"></script>
    <script type="text/javascript" src="/js/custom2.js"></script>
<script type="text/javascript">
    Date.createFromMysql = function(mysql_string)
    {
        if(typeof mysql_string === 'string')
        {
            var t = mysql_string.split(/[- :]/);

            //when t[3], t[4] and t[5] are missing they defaults to zero
            return new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0);
        }
        return null;
    }

    createStoryJS({
        type:       'timeline',
        width:      '100%',
        height:     '600',
        //  source:     '/js/timeline.json',
        source:    {
            "timeline":

      {
                "headline":"{{$service->label}}",
                "type":"default",
                "text":"<p>Welcome</p>",
                "asset": {

                    "credit":"Credit Name Goes Here",
                    "caption":"Caption text goes here"
                },
          "date": [
                        @foreach($servicesContainer as $serviceContainer)
               @foreach($serviceContainer as $service)
                         {
                        "startDate":new Date.createFromMysql('{{$service->datetime_from}}'),
                        "endDate":new Date.createFromMysql('{{$service->datetime_to}}'),
                        "headline":"{{$service->label}}",
                        "text":"<p>{{$service->label}}</p>",
                        {{--"tag":"{{$service->label}}",--}}
                        "classname":"optionaluniqueclassnamecanbeaddedhere",
                        "asset": {
                            "credit":"Credit Name Goes Here",
                            "caption":"Caption text goes here"
                        }
                    },
                    @endforeach
                 @endforeach
           ],
          "era": [
                        @foreach($servicesContainer as $serviceContainer)
                      @foreach($serviceContainer as $service)
                      {
                        "startDate":new Date.createFromMysql('{{$service->datetime_from}}'),
                        "endDate":new Date.createFromMysql('{{$service->datetime_to}}'),
                      //  "headline":"{{$service->label}}",
                        "text":"<p>Second</p>",
                        "tag":"We Three process timeline"
                    },

                    @endforeach
                  @endforeach
              ]

      }

   },
        embed_id:   'timeline-embed'
    });	</script>
</body>
</html>
