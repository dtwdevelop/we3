@extends('app')

@section('content')
    <div class="page-header">
        <h1>Projects / Create </h1>
    </div>


    <div class="row">
        <div class="col-md-6">

            <form action="{{ route('admin.projects.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                     <label for="title">TITLE</label>
                     <input type="text" name="title" class="form-control" value=""/>
                </div>
                <div class="form-group">
                     <label for="title">Contact name</label>
                       {!! Form::select('project_id', $projects) !!}
                </div>



            <a class="btn btn-default" href="{{ route('admin.projects.index') }}">Back</a>
            <button class="btn btn-primary" type="submit" >Create</a>
            </form>
        </div>
    </div>


@endsection