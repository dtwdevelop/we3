@extends('app')

@section('content')
    <div class="page-header">
        <h1>Projects</h1>
    </div>


    <div class="row">
        <div class="col-md-6">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>TITLE</th>
                        <th class="text-right">OPTIONS</th>
                    </tr>
                </thead>

                <tbody>

                @foreach($projects as $project)
                <tr>
                    <td>{{$project->id}}</td>
                    <td>{{$project->title}}</td>

                    <td class="text-right">
                        <a class="btn btn-warning " href="{{ route('admin.projects.edit', $project->id) }}">Edit</a>
                        <form action="{{ route('admin.projects.destroy', $project->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="_token" value="{{ csrf_token() }}"> <button class="btn btn-danger" type="submit">Delete</button></form>
                    </td>
                </tr>

                @endforeach

                </tbody>
            </table>

            <a class="btn btn-success" href="{{ route('admin.projects.create') }}">Create</a>
           <a class="btn btn-warning" href="{{ route('admin.timelines.index') }}">Timelines</a>
           <a class="btn btn-danger" href="{{ route('admin.timelines.create') }}">Create Timelines</a>

        </div>
    </div>


@endsection