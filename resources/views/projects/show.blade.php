@extends('layout')

@section('content')
    <div class="page-header">
        <h1>Projects / Show </h1>
    </div>


    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static">{{$project->id}}</p>
                </div>
                <div class="form-group">
                     <label for="title">TITLE</label>
                     <p class="form-control-static">{{$project->title}}</p>
                </div>
            </form>



            <a class="btn btn-default" href="{{ route('projects.index') }}">Back</a>
            <a class="btn btn-warning" href="{{ route('projects.edit', $project->id) }}">Edit</a>
            <form action="#/$project->id" method="DELETE" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };"><button class="btn btn-danger" type="submit">Delete</button></form>
        </div>
    </div>


@endsection